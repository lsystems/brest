'use strict';

var gulp = require('gulp');
var webpackS = require('webpack-stream');
var webpack = require('webpack');
var mocha = require('gulp-mocha');
var cover = require('gulp-coverage');
var jshint = require('gulp-jshint');
var jscs = require('gulp-jscs');
var stylish = require('gulp-jscs-stylish');
var jsdoc = require('gulp-jsdoc3');
var gutil = require('gulp-util');
var jsdocToMarkdown = require('gulp-jsdoc-to-markdown');
var concat = require('gulp-concat');

gulp.task('watch', function () {
  gulp.watch(['BRest.js', 'lib/**/!(*.spec).js'], ['codestyle', 'test', 'build']);
  gulp.watch(['BRest.js', 'lib/**/*.spec.js'], ['codestyle', 'test']);
});

gulp.task('build', ['codestyle', 'test'], function () {
  return gulp.src('BRest.js')
    .pipe(webpackS({
      output: {
        libraryTarget: 'var',
        library: 'BRest',
        filename: 'BRest.js',
      },
      externals: {
        lodash: '_',
        bluebird: 'Promise',
      },
      devtool: 'source-map',
    }))
    .pipe(gulp.dest('dist/'))
  ;
});

gulp.task('copy-commons', function() {
  return gulp.src('package.json', 'README.md')
    .pipe(gulp.dest('dist/'))
  ;
});

gulp.task('build-all', ['build', 'copy-commons'], function () {
  return gulp.src('BRest.js')
    .pipe(webpackS({
      output: {
        libraryTarget: 'var',
        library: 'BRest',
        filename: 'BRest.min.js',
      },
      externals: {
        lodash: '_',
        bluebird: 'Promise',
      },
      devtool: 'source-map',
      plugins: [
        new webpack.optimize.UglifyJsPlugin({minimize: true})
      ]
    }))
    .pipe(gulp.dest('dist/'))
  ;
});

gulp.task('doc', function() {
  gulp.src(['BRest.js', 'lib/**/!(*.spec).js'])
    .pipe(concat('API.md'))
    .pipe(jsdocToMarkdown())
    .on('error', function (err) {
      gutil.log('jsdoc2md failed:', err.message)
    })
    .pipe(gulp.dest('.'))
  ;
});

gulp.task('html-doc', function(cb) {
  gulp.src(['package.json', 'README.md', 'BRest.js', 'lib/**/!(*.spec).js'], {read: false})
    .pipe(jsdoc(require('./utils/jsdocConfig'), cb))
  ;
});

gulp.task('default', ['codestyle', 'test', 'build', 'watch']);

gulp.task('test', function() {
  return gulp.src(['lib/**/*.spec.js'], { read: false })
    .pipe(mocha({ reporter: 'list' }));
});

gulp.task('cov', function() {
  return gulp.src(['lib/**/*.spec.js'], { read: false })
    .pipe(cover.instrument({
      pattern: ['lib/**/!(*.spec).js'],
      debugDirectory: 'debug'
    }))
    .pipe(mocha())
    .pipe(cover.gather())
    .pipe(cover.format())
    .pipe(gulp.dest('reports'));
});

gulp.task('codestyle', function() {
  return gulp.src(['BRest.js', 'lib/**/*.js'])
    .pipe(jshint())
    .pipe(jscs())
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter('jshint-stylish'));
});
