/**
 * BRest - Browser REST
 * Simply use REST v2, v3 or v4 API
 * @module BRest
 */
'use strict';

module.exports = function () {
  var BRest = {};

  require('./lib/request')(BRest);
  require('./lib/static')(BRest);
  require('./lib/restResource')(BRest);
  require('./lib/v2RestResource')(BRest);
  require('./lib/v3RestResource')(BRest);
  require('./lib/v4RestResource')(BRest);

  return BRest;
}();
