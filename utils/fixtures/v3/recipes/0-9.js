'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      self: 'http://localhost:9000/recipes',
      last: 'http://localhost:9000/recipes?type=recipe&start=30',
      next: 'http://localhost:9000/recipes?type=recipe&start=10',
      elements: [
        'http://localhost:9000/recipes/mousse-chocolat',
        'http://localhost:9000/recipes/tarte-citron-meringuee',
        'http://localhost:9000/recipes/tarte-pomme',
        'http://localhost:9000/recipes/sorbet-fraise',
        'http://localhost:9000/recipes/sable-cannelle',
        'http://localhost:9000/recipes/millefeuille',
        'http://localhost:9000/recipes/pate-amande',
        'http://localhost:9000/recipes/galette-rois',
        'http://localhost:9000/recipes/ile-flottante',
        'http://localhost:9000/recipes/foret-noire',
      ],
      count: 31,
    },
    statusCode: 206,
  };
};
