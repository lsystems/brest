'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=5',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      self: 'http://localhost:9000/recipes?type=recipe&size=5',
      last: 'http://localhost:9000/recipes?type=recipe&start=30&size=5',
      next: 'http://localhost:9000/recipes?type=recipe&start=5&size=5',
      elements: [
        'http://localhost:9000/recipes/mousse-chocolat',
        'http://localhost:9000/recipes/tarte-citron-meringuee',
        'http://localhost:9000/recipes/tarte-pomme',
        'http://localhost:9000/recipes/sorbet-fraise',
        'http://localhost:9000/recipes/sable-cannelle',
      ],
      count: 31,
    },
    statusCode: 206,
  };
};
