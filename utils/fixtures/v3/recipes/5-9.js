'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=5&size=5',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: '{' +
      '"self": "http://localhost:9000/recipes?type=recipe&start=5&size=5",' +
      '"last": "http://localhost:9000/recipes?type=recipe&start=30&size=5",' +
      '"next": "http://localhost:9000/recipes?type=recipe&start=10&size=5",' +
      '"elements": [' +
      '  "http://localhost:9000/recipes/millefeuille",' +
      '  "http://localhost:9000/recipes/pate-amande",' +
      '  "http://localhost:9000/recipes/galette-rois",' +
      '  "http://localhost:9000/recipes/ile-flottante",' +
      '  "http://localhost:9000/recipes/foret-noire"' +
      '],' +
      '"count": 31' +
    '}',
    statusCode: 206,
  };
};
