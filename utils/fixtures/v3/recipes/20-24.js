'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=20&size=5',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      self: 'http://localhost:9000/recipes?type=recipe&start=20&size=5',
      first: 'http://localhost:9000/recipes?type=recipe&start=0&size=5',
      last: 'http://localhost:9000/recipes?type=recipe&start=30&size=5',
      prev: 'http://localhost:9000/recipes?type=recipe&start=15&size=5',
      next: 'http://localhost:9000/recipes?type=recipe&start=25&size=5',
      elements: [
        'http://localhost:9000/recipes/gateau-ananas',
        'http://localhost:9000/recipes/gateau-pomme',
        'http://localhost:9000/recipes/brochettes-fruit',
        'http://localhost:9000/recipes/pomme-four',
        'http://localhost:9000/recipes/muffin-pomme',
      ],
      count: 31,
    },
    statusCode: 206,
  };
};
