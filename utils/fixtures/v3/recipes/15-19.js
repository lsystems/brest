'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=15&size=5',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: '{' +
      '"self": "http://localhost:9000/recipes?type=recipe&start=15&size=5",' +
      '"last": "http://localhost:9000/recipes?type=recipe&start=30&size=5",' +
      '"next": "http://localhost:9000/recipes?type=recipe&start=20&size=5",' +
      '"elements": [' +
        '"http://localhost:9000/recipes/fondant-chocolat",' +
        '"http://localhost:9000/recipes/cookies",' +
        '"http://localhost:9000/recipes/brownie",' +
        '"http://localhost:9000/recipes/tiramisu",' +
        '"http://localhost:9000/recipes/amandine-framboise"' +
      '],' +
      '"count": 31' +
    '}',
    statusCode: 206,
  };
};
