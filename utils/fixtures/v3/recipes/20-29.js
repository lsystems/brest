'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=20',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      self: 'http://localhost:9000/recipes?type=recipe&start=20',
      first: 'http://localhost:9000/recipes?type=recipe&start=0',
      last: 'http://localhost:9000/recipes?type=recipe&start=30',
      prev: 'http://localhost:9000/recipes?type=recipe&start=10',
      next: 'http://localhost:9000/recipes?type=recipe&start=30',
      elements: [
        'http://localhost:9000/recipes/gateau-ananas',
        'http://localhost:9000/recipes/gateau-pomme',
        'http://localhost:9000/recipes/brochettes-fruit',
        'http://localhost:9000/recipes/pomme-four',
        'http://localhost:9000/recipes/muffin-pomme',
        'http://localhost:9000/recipes/clafoutis-cerise',
        'http://localhost:9000/recipes/charlotte-framboise',
        'http://localhost:9000/recipes/tarte-pomme-normande',
        'http://localhost:9000/recipes/tarte-fraise',
        'http://localhost:9000/recipes/tarte-noix',
      ],
      count: 31,
    },
    statusCode: 206,
  };
};
