'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=10',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: '{' +
      '"self": "http://localhost:9000/recipes?type=recipe&start=10",' +
      '"last": "http://localhost:9000/recipes?type=recipe&start=30",' +
      '"next": "http://localhost:9000/recipes?type=recipe&start=20",' +
      '"elements": [' +
        '"http://localhost:9000/recipes/moelleux-chocolat",' +
        '"http://localhost:9000/recipes/gateau-marbre",' +
        '"http://localhost:9000/recipes/crepe",' +
        '"http://localhost:9000/recipes/gaufre",' +
        '"http://localhost:9000/recipes/madeleine",' +
        '"http://localhost:9000/recipes/fondant-chocolat",' +
        '"http://localhost:9000/recipes/cookies",' +
        '"http://localhost:9000/recipes/brownie",' +
        '"http://localhost:9000/recipes/tiramisu",' +
        '"http://localhost:9000/recipes/amandine-framboise"' +
      '],' +
      '"count": 31' +
    '}',
    statusCode: 206,
  };
};
