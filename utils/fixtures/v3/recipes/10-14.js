'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=10&size=5',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      self: "http://localhost:9000/recipes?type=recipe&start=10&size=5",
      last: "http://localhost:9000/recipes?type=recipe&start=30&size=5",
      next: "http://localhost:9000/recipes?type=recipe&start=15&size=5",
      elements: [
        'http://localhost:9000/recipes/moelleux-chocolat',
        'http://localhost:9000/recipes/gateau-marbre',
        'http://localhost:9000/recipes/crepe',
        'http://localhost:9000/recipes/gaufre',
        'http://localhost:9000/recipes/madelein',
      ],
      count: 31,
    },
    statusCode: 206,
  };
};
