'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=25&size=5',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      self: 'http://localhost:9000/recipes?type=recipe&start=25&size=5',
      first: 'http://localhost:9000/recipes?type=recipe&start=0&size=5',
      last: 'http://localhost:9000/recipes?type=recipe&start=30&size=5',
      prev: 'http://localhost:9000/recipes?type=recipe&start=20&size=5',
      next: 'http://localhost:9000/recipes?type=recipe&start=30&size=5',
      elements: [
        'http://localhost:9000/recipes/clafoutis-cerise',
        'http://localhost:9000/recipes/charlotte-framboise',
        'http://localhost:9000/recipes/tarte-pomme-normande',
        'http://localhost:9000/recipes/tarte-fraise',
        'http://localhost:9000/recipes/tarte-noix',
      ],
      count: 31,
    },
    statusCode: 206,
  };
};
