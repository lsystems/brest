'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&start=10',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      self: 'http://localhost:9000/recipes?type=recipe&start=30',
      first: 'http://localhost:9000/recipes?type=recipe&start=0',
      prev: 'http://localhost:9000/recipes?type=recipe&start=20',
      elements: [
        'http://localhost:9000/recipes/tarte-tatin',
      ],
      count: 31,
    },
    statusCode: 206,
  };
};
