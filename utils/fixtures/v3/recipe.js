'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipes/' + id,
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-' + id,
    },
    body: {
      name: 'Recipe name ' + id,
      description: 'Recipe description ' + id,
      id: id,
      thumbnailUrl: '/recipes/' + id + '.jpg',
      _ingredients: 'http://Nelim-PC:5000/recipes/' + id + '/ingredients',
      _rate: 'http://Nelim-PC:5000/recipes/' + id + '/rate',
      _comment: 'http://Nelim-PC:5000/recipes/' + id + '/comment',
      '@context': 'http://schema.org/',
      '@type': 'Recipe'
    }
  };
};
