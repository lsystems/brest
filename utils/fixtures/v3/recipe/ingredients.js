'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipes/' + id + '/ingredients',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-' + id + '-ingredients',
    },
    body: {
      self: 'http://localhost:9000/recipes/' + id + '/ingredients',
      elements: [
        'http://localhost:9000/ingredients/chocolat',
        'http://localhost:9000/ingredients/oeuf',
        'http://localhost:9000/ingredients/creme',
      ],
      count: 3,
    },
    statusCode: 206,
  };
};
