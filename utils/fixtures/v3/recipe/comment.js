'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipes/' + id + '/comment',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-' + id + '-comment',
    },
    body: {
      text: 'I\'m the comment of recipe n°' + id,
      '_author': 'http://localhost:9000/author/vbardales',
      '@context': 'http://schema.org/',
      '@type': 'Comment',
    },
  };
};
