'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-summary',
    },
    body: {
      self: 'http://localhost:9000',
      recipes: 'http://localhost:9000/recipes',
      ingredients: 'http://localhost:9000/ingredients',
    },
  };
};
