'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=5&start=15',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 15,
      count: 5,
      total: 31,
      type: 'recipe',
      elements: [
        'fondant-chocolat',
        'cookies',
        'brownie',
        'tiramisu',
        'amandine-framboise',
      ],
    },
    statusCode: 206,
  };
};
