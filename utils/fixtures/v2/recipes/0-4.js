'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=5&start=0',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 0,
      count: 5,
      total: 31,
      type: 'recipe',
      elements: [
        'mousse-chocolat',
        'tarte-citron-meringuee',
        'tarte-pomme',
        'sorbet-fraise',
        'sable-cannelle',
      ],
    },
    statusCode: 206,
  };
};
