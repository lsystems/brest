'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=5&start=25',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 25,
      count: 5,
      total: 31,
      type: 'recipe',
      elements: [
        'clafoutis-cerise',
        'charlotte-framboise',
        'tarte-pomme-normande',
        'tarte-fraise',
        'tarte-noix',
      ],
    },
    statusCode: 206,
  };
};
