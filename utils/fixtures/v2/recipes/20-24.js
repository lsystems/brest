'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=5&start=20',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 20,
      count: 5,
      total: 31,
      type: 'recipe',
      elements: [
        'gateau-ananas',
        'gateau-pomme',
        'brochettes-fruit',
        'pomme-four',
        'muffin-pomme',
      ],
    },
    statusCode: 206,
  };
};
