'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 5,
      count: 5,
      total: 31,
      type: 'recipe',
      elements: [
        'millefeuille',
        'pate-amande',
        'galette-rois',
        'ile-flottante',
        'foret-noire',
      ]
    },
    statusCode: 206,
  };
};
