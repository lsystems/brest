'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=5&start=10',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 10,
      count: 5,
      total: 31,
      type: 'recipe',
      elements: [
        'moelleux-chocolat',
        'gateau-marbre',
        'crepe',
        'gaufre',
        'madeleine',
      ],
    },
    statusCode: 206,
  };
};
