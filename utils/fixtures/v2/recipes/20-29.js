'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=10&start=20',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 20,
      count: 10,
      total: 31,
      type: 'recipe',
      elements: [
        'gateau-ananas',
        'gateau-pomme',
        'brochettes-fruit',
        'pomme-four',
        'muffin-pomme',
        'clafoutis-cerise',
        'charlotte-framboise',
        'tarte-pomme-normande',
        'tarte-fraise',
        'tarte-noix',
      ]
    },
    statusCode: 206,
  };
};
