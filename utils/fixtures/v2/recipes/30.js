'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes?type=recipe&size=10&start=20',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 30,
      count: 10,
      total: 31,
      type: 'recipe',
      elements: [
        'tarte-tatin',
      ]
    },
    statusCode: 206,
  };
};
