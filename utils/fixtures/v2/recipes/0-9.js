'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-recipes',
    },
    body: {
      start: 0,
      count: 10,
      total: 31,
      type: 'recipe',
      elements: [
        'mousse-chocolat',
        'tarte-citron-meringuee',
        'tarte-pomme',
        'sorbet-fraise',
        'sable-cannelle',
        'millefeuille',
        'pate-amande',
        'galette-rois',
        'ile-flottante',
        'foret-noire',
      ]
    },
    statusCode: 206,
  };
};
