'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/ingredients/' + id,
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-' + id,
    },
    body: {
      name: 'Ingredient name ' + id,
      description: 'Ingredient description ' + id,
      id: id,
      thumbnailUrl: '/ingredients/' + id + '.jpg',
      '@context': 'http://schema.org/',
      '@type': 'Ingredient',
    },
  };
};
