'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipes/' + id,
    headers: {
      'Content-Type': 'application/json',
      ETag: 'version-' + id,
    },
    body: {
      name: 'Recipe name ' + id,
      description: 'Recipe description ' + id,
      id: id,
      thumbnailUrl: '/recipes/' + id + '.jpg',
      ingredients: [
        'chocolat',
        'oeuf',
        'creme',
      ],
      rate: {
        value: 5,
        text: 'cinq',
        id: 5,
        imageUrl: '/rate/5.jpg',
        '@context': 'http://schema.org/',
        '@type': 'Rate',
      },
      comment: {
        text: 'I\'m the comment of recipe n°' + id,
        'author': {
          id: 'vbardales',
        },
        '@context': 'http://schema.org/',
        '@type': 'Comment',
      },
      '@context': 'http://schema.org/',
      '@type': 'Recipe'
    }
  };
};
