'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipes/' + id,
    headers: {
      'Content-Type': 'application/vnd.api-benchmark.recipe+ld+json',
      'Link': '<http://localhost:9000/recipes/' + id + '/ingredients>; rel="ingredients", ' +
        '<http://localhost:9000/recipes/' + id + '/rate>; rel="rate", ' +
        '<http://localhost:9000/recipes/' + id + '/comment>; rel="comment"',
      ETag: 'version-' + id,
    },
    body: {
      name: 'Recipe name ' + id,
      description: 'Recipe description ' + id,
      id: id,
      thumbnailUrl: '/recipes/' + id + '.jpg',
      '@context': 'http://schema.org/',
      '@type': 'Recipe',
    },
  };
};
