'use strict';

module.exports = function(xhr) {
  return {
    responseUrl: 'http://localhost:9000',
    headers: {
      'Content-Type': 'application/vnd.api-benchmark.summary+json',
      ETag: 'version-summary',
      'Link': '<http://localhost:9000/recipes>; rel="recipes", <http://localhost:9000/ingredients>; ' +
        'rel="ingredients"',
    },
    body: '{"version":"1.0"}',
  };
};
