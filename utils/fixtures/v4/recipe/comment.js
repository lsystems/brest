'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipes/' + id + '/comment',
    headers: {
      'Content-Type': 'application/vnd.api-benchmark.comment+ld+json',
      'Link': '<http://localhost:9000/author/vbardales>; rel="author"',
      ETag: 'version-' + id + '-comment',
    },
    body: {
      text: 'I\'m the comment of recipe n°' + id,
      '@context': 'http://schema.org/',
      '@type': 'Comment',
    },
  };
};
