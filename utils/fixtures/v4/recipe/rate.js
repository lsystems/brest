'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipe/' + id + '/rate',
    Headers: {
      Location: 'http://localhost:9000/rate/5',
    },
    statusCode: 303,
  };
};
