'use strict';

module.exports = function(id) {
  return {
    responseUrl: 'http://localhost:9000/recipes/' + id + '/ingredients',
    headers: {
      'Content-Range': 'ingredient 0-2/3',
      'Content-Type': 'application/vnd.collection+json',
      ETag: 'version-' + id + '-ingredients',
      'Link': '<http://localhost:9000/ingredients/chocolat>; rel="0", ' +
        '<http://localhost:9000/ingredients/oeuf>; rel="1", ' +
        '<http://localhost:9000/ingredients/creme>; rel="2"',
    },
    body: '{"version":"1.0"}',
    statusCode: 206,
  };
};
