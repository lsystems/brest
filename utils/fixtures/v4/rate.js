'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/rate/5',
    headers: {
      'Content-Type': 'application/vnd.api-benchmark.rate+ld+json',
      ETag: 'version-rate-5',
    },
    body: {
      value: 5,
      text: 'cinq',
      id: 5,
      imageUrl: '/rate/5.jpg',
      '@context': 'http://schema.org/',
      '@type': 'Rate',
    },
  };
};
