'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Range': 'recipe 5-9/31',
      'Content-Type': 'application/vnd.collection+json',
      ETag: 'version-recipes',
      'Link': '<http://localhost:9000/recipes/millefeuille>; rel="5", ' +
        '<http://localhost:9000/recipes/pate-amande>; rel="6", ' +
        '<http://localhost:9000/recipes/galette-rois>; rel="7", ' +
        '<http://localhost:9000/recipes/ile-flottante>; rel="8", ' +
        '<http://localhost:9000/recipes/foret-noire>; rel="9"',
    },
    body: '{"version":"1.0"}',
    statusCode: 206,
  };
};
