'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Range': 'recipe 30/31',
      'Content-Type': 'application/vnd.collection+json',
      ETag: 'version-recipes',
      'Link': '<http://localhost:9000/recipes/tarte-tatin>; rel="30"',
    },
    body: '{"version":"1.0"}',
    statusCode: 206,
  };
};
