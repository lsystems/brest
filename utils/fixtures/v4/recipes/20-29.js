'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Range': 'recipe 20-29/31',
      'Content-Type': 'application/vnd.collection+json',
      ETag: 'version-recipes',
      'Link': '<http://localhost:9000/recipes/gateau-ananas>; rel="20", ' +
        '<http://localhost:9000/recipes/gateau-pomme>; rel="21", ' +
        '<http://localhost:9000/recipes/brochettes-fruit>; rel="22", ' +
        '<http://localhost:9000/recipes/pomme-four>; rel="23", ' +
        '<http://localhost:9000/recipes/muffin-pomme>; rel="24", ' +
        '<http://localhost:9000/recipes/clafoutis-cerise>; rel="25", ' +
        '<http://localhost:9000/recipes/charlotte-framboise>; rel="26", ' +
        '<http://localhost:9000/recipes/tarte-pomme-normande>; rel="27", ' +
        '<http://localhost:9000/recipes/tarte-fraise>; rel="28", ' +
        '<http://localhost:9000/recipes/tarte-noix>; rel="29"',
    },
    body: '{"version":"1.0"}',
    statusCode: 206,
  };
};
