'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Range': 'recipe 10-14/31',
      'Content-Type': 'application/vnd.collection+json',
      ETag: 'version-recipes',
      'Link': '<http://localhost:9000/recipes/moelleux-chocolat>; rel="10", ' +
        '<http://localhost:9000/recipes/gateau-marbre>; rel="11", ' +
        '<http://localhost:9000/recipes/crepe>; rel="12", ' +
        '<http://localhost:9000/recipes/gaufre>; rel="13", ' +
        '<http://localhost:9000/recipes/madeleine>; rel="14"',
    },
    body: '{"version":"1.0"}',
    statusCode: 206,
  };
};
