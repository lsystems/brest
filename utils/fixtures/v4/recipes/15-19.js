'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Range': 'recipe 15-19/31',
      'Content-Type': 'application/vnd.collection+json',
      ETag: 'version-recipes',
      'Link': '<http://localhost:9000/recipes/fondant-chocolat>; rel="15", ' +
        '<http://localhost:9000/recipes/cookies>; rel="16", ' +
        '<http://localhost:9000/recipes/brownie>; rel="17", ' +
        '<http://localhost:9000/recipes/tiramisu>; rel="18", ' +
        '<http://localhost:9000/recipes/amandine-framboise>; rel="19"',
    },
    body: '{"version":"1.0"}',
    statusCode: 206,
  };
};
