'use strict';

module.exports = function() {
  return {
    responseUrl: 'http://localhost:9000/recipes',
    headers: {
      'Content-Range': 'recipe 0-4/31',
      'Content-Type': 'application/vnd.collection+json',
      ETag: 'version-recipes',
      'Link': '<http://localhost:9000/recipes/mousse-chocolat>; rel="0", ' +
        '<http://localhost:9000/recipes/tarte-citron-meringuee>; rel="1", ' +
        '<http://localhost:9000/recipes/tarte-pomme>; rel="2", ' +
        '<http://localhost:9000/recipes/sorbet-fraise>; rel="3", ' +
        '<http://localhost:9000/recipes/sable-cannelle>; rel="4"',
    },
    body: '{"version":"1.0"}',
    statusCode: 206,
  };
};
