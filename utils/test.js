'use strict';

var chai = require('chai');
var sinon = require('sinon');
global.XMLHttpRequest = sinon.useFakeXMLHttpRequest();
var expect = chai.expect;
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));
// order is important, it MUST be before dirty-chai
chai.use(require('chai-as-promised'));
chai.use(require('dirty-chai'));

var path = require('path');

module.exports = function(fixtureName) {
  Array.prototype.unshift.call(arguments, __dirname, './fixtures/');
  return require(path.join.apply(path, arguments));
};
