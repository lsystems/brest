'use strict';

var _ = require('lodash');

module.exports = function(Xhr) {
  var RequestStub = function RequestStub(options) {
    return RequestStub.Request.fromXMLHttpRequest(options instanceof XMLHttpRequest ? options : Xhr(options));
  };

  RequestStub.Request = null;

  // proxy xhr
  _.forIn(Xhr, function(prop, name) {
    if (_.isFunction(prop)) {
      RequestStub[name] = prop.bind(Xhr);
      return;
    }

    Object.defineProperty(RequestStub, name, {
      get: function() {
        return Xhr[name];
      },
      set: function(val) {
        Xhr[name] = val;
      },
    })
  });

  Object.defineProperties(RequestStub, {
    xhttp: {
      get: function() {
        return new RequestStub(Xhr.xhttp);
      },
    },
  });

  RequestStub.setRequest = function(Request) {
    RequestStub.Request = Request;
  };

  return RequestStub;
};
