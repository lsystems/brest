'use strict';

var _ = require('lodash');

var defaults = {};

var Params = function(options) {
  options = options || {};

  this.responseUrl = options.responseUrl || options.requestUrl || defaults.responseUrl;
  this.headers = _.merge({}, defaults.headers, options.headers);
  this.headers['Content-Location'] = this.responseUrl;
  this.statusCode = options.statusCode || defaults.statusCode;
  var body = options.body || defaults.body;
  this.body = body && !_.isString(body) ? JSON.stringify(body) : body;
};

defaults = new Params({
  statusCode: 200,
  headers: {},
  body: undefined,
});

Params.prototype.fromObject = function(options) {
  options = options || {};

  this.responseUrl = options.responseUrl || options.requestUrl || this.responseUrl;
  this.headers = _.merge(this.headers, options.headers);
  this.headers['Content-Location'] = this.responseUrl;
  this.statusCode = options.statusCode || this.statusCode;
  this.body = options.body && !_.isString(options.body) ? JSON.stringify(options.body) : options.body || this.body;
  return this;
};

Params.prototype.setStatusCode = function(statusCode) {
  this.statusCode = statusCode;
  return this;
};

Params.prototype.setHeaders = function(headers) {
  if (arguments.length > 1) {
    throw new Error('XhrStub: Use setHeader instead');
  }
  _.merge(this.headers, headers);
  return this;
};

Params.prototype.setHeader = function(headerName, value) {
  if (arguments.length <= 1) {
    throw new Error('XhrStub: Use unsetHeader or setHeaders instead');
  }
  this.headers[headerName] = value;
  return this;
};

Params.prototype.unsetHeader = function(headerName) {
  if (arguments.length > 1) {
    throw new Error('XhrStub: Use setHeader instead');
  }
  delete this.headers[headerName];
  return this;
};

Params.prototype.setBody = function(body) {
  this.body = body && !_.isString(body) ? JSON.stringify(body) : body;
  return this;
};

Params.prototype.setResponseUrl = function(responseUrl) {
  this.responseUrl = responseUrl;
  this.headers['Content-Location'] = responseUrl;
  return this;
};
Params.prototype.setRequestUrl = Params.prototype.setResponseUrl;

Params.prototype.get = function(name) {
  if (name === 'requestUrl') {
    name = 'responseUrl';
  }
  return this[name];
};

var Xhr = function(options) {
  if (!options && Xhr.batch.length === 0) {
    throw new Error('XhrStub: No data loaded for response');
  }

  var params = Xhr.batch.shift() || new Params(options);
  if (!params.responseUrl) {
    throw new Error('XhrStub: No response url set, have you executed your fixtures?');
  }

  var xhttp = new XMLHttpRequest();
  xhttp.open('GET', params.responseUrl);
  xhttp.respond(params.statusCode, params.headers, params.body);

  // FakeXMLHttp doesn't set responseURL (?)
  xhttp.responseURL = params.responseUrl;

  Xhr.lastCalledParams = params;

  return xhttp;
};

_.merge(Xhr, {
  batch: [],
  setBatch: function(batchData) {
    this.batch = _.concat(this.batch, _.map(!_.isArray(batchData) ? [batchData] : batchData, function(batch) {
      return (batch instanceof Params) ? batch : new Params(batch);
    }));
  },
  clearBatch: function(){
    this.batch = [];
  },
  defaults: defaults,
  params: new Params(),
  reset: function() {
    this.params = new Params();
  },
  setParamsObject: function(obj) {
    this.params = new Params(obj);
  },
  setDefaultsObject: function(obj) {
    this.defaults.fromObject(obj);
  },
});

// proxy params
_.forIn(Xhr.params, function(fn, fnName) {
  if (!_.isFunction(fn)) {
    return;
  }
  Xhr[fnName] = function() {
    return this.params[fnName].apply(this.params, arguments);
  };
});

Object.defineProperties(Xhr, {
  xhttp: {
    get: function() {
      this.batch.unshift(this.params);
      return new Xhr();
    },
  },
});

module.exports = Xhr;
