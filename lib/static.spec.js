'use strict';

var chai = require('chai');
var sinon = require('sinon');
var expect = chai.expect;
var assert = chai.assert;
var Promise = require('bluebird');
var xhr = require('../utils/requestForXhrStub')(require('../utils/xhrStub'));
var fixture = require('../utils/test');

var staticModule = require('./static');

describe('module static', function() {
  var RequestModule = require('./request');
  var BRest = {
    v2: {
      RestResource: function() {
        this.version = 'v2';
      },
    },
    v3: {
      RestResource: function() {
        this.version = 'v3';
      },
    },
    v4: {
      RestResource: function() {
        this.version = 'v4';
      },
    },
  };

  before(function() {
    RequestModule(BRest);
    xhr.setRequest(BRest.Request);
  });

  beforeEach(function() {
    sinon.spy(BRest.v2, 'RestResource');
    sinon.spy(BRest.v3, 'RestResource');
    sinon.spy(BRest.v4, 'RestResource');
  });

  afterEach(function() {
    BRest.v2.RestResource.restore();
    BRest.v3.RestResource.restore();
    BRest.v4.RestResource.restore();
  });

  it('should be a function', function() {
    expect(staticModule).to.be.a('Function');
  });

  it('should return nothing', function() {
    expect(staticModule(BRest)).to.be.undefined();
  });

  it('should define RestApi', function() {
    staticModule(BRest);
    expect(BRest.RestApi).to.be.a('Function');
  });

  describe('RestApi', function() {
    before(function() {
      staticModule(BRest);
      xhr.defaults.setHeader('server', 'ApiBenchmark');

      BRest.$sendRequest = function(method, url, options) {
        return Promise.resolve(xhr());
      };
    });

    beforeEach(function() {
      sinon.spy(BRest, '$sendRequest');
      xhr.reset();
    });

    afterEach(function() {
      BRest.$sendRequest.restore();
    });

    it('should throw an error if no url has been given and no rootUrl in the options', function() {
      var thrown = function() {
        BRest.RestApi({});
      };
      expect(thrown).to.throw('No url found');
    });

    it('should return a v2 resource if a rootUrl has been given in the options, even if an url exist', function() {
      var res = BRest.RestApi('url', {
        rootUrl: 'rootUrlValue',
        test: 'test',
      });
      expect(BRest.v2.RestResource).to.have.been.calledWith('rootUrlValue', {test: 'test'});
      return expect(res).to.eventually.have.a.property('version').that.equal('v2');
    });

    it('should send a request on the root URL to determine its version', function() {
      xhr.setBatch(fixture('v4', 'summary')());
      var res = BRest.RestApi('http://localhost:9000', { test: 'test' });
      expect(BRest.$sendRequest).to.have.been.calledWith('GET', 'http://localhost:9000', {test: 'test'});
    });

    it('should return a v4 resource if a Link header exists', function() {
      xhr.setBatch(fixture('v4', 'summary')());
      return expect(BRest.RestApi('http://localhost:9000', { test: 'test' })).to.eventually.have.a.property('version').
        that.equal('v4');
    });

    it('should return a v3 resource if a response has been found', function() {
      xhr.setBatch(fixture('v3', 'summary')());
      return expect(BRest.RestApi('http://localhost:9000', { test: 'test' })).to.eventually.have.a.property('version').
        that.equal('v3');
    });

    it('should return a v2 resource if no response', function() {
      xhr.params.setResponseUrl('http://localhost:9000');
      xhr.setBatch(xhr.params);
      return expect(BRest.RestApi('http://localhost:9000', { test: 'test' })).to.eventually.have.a.property('version').
        that.equal('v2');
    });

    it('should return a v2 resource if the HTTP request fails (status: 0, due to CORS)', function() {
      xhr.params.setResponseUrl('http://localhost:9000');
      BRest.$sendRequest = function(method, url, options) {
        var error = new Error();
        error.xhttp = xhr(xhr.params);
        error.xhttp.status = 0;
        return Promise.reject(error);
      };
      sinon.spy(BRest, '$sendRequest');
      return expect(BRest.RestApi('http://localhost:9000', { test: 'test' })).to.eventually.have.a.property('version').
        that.equal('v2');
    });

    it('should return a v2 resource if the HTTP request fails (status: 404)', function() {
      xhr.params.setResponseUrl('http://localhost:9000');
      BRest.$sendRequest = function(method, url, options) {
        var error = new Error();
        error.xhttp = xhr(xhr.params);
        error.xhttp.status = 404;
        return Promise.reject(error);
      };
      sinon.spy(BRest, '$sendRequest');
      return expect(BRest.RestApi('http://localhost:9000', { test: 'test' })).to.eventually.have.a.property('version').
        that.equal('v2');
    });

    it('should return the error otherwise', function() {
      xhr.params.setResponseUrl('http://localhost:9000');
      BRest.$sendRequest = function(method, url, options) {
        return Promise.reject(new Error());
      };
      sinon.spy(BRest, '$sendRequest');
      return expect(BRest.RestApi('http://localhost:9000', { test: 'test' })).to.eventually.be.an.instanceOf(Error);
    });
  });

  it('should define $initOnDemand', function() {
    staticModule(BRest);
    expect(BRest.$initOnDemand).to.be.a('Function');
  });

  describe('$initOnDemand', function() {
    before(function() {
      staticModule(BRest);
    });

    it('should define the definition object key on the destination object', function() {
      var dest = {};
      var definition = {
        key1: function() {},
        key2: function() {},
      };
      BRest.$initOnDemand(dest, definition);
      expect(dest).to.have.property('key1');
      expect(dest).to.have.property('key2');
    });

    it('should not call the definition object value functions before access', function() {
      var dest = {};
      var definition = {
        key1: sinon.spy(),
        key2: sinon.spy(),
      };
      BRest.$initOnDemand(dest, definition);
      expect(definition.key1).not.to.have.been.calledOnce();
      expect(definition.key2).not.to.have.been.calledOnce();
    });

    it('should define the definition object key on the definition object value once invocated once accessed',
      function() {
        var dest = {};
        var definition = {
          key1: sinon.spy(),
          key2: sinon.spy(),
        };
        BRest.$initOnDemand(dest, definition);
        var key1 = dest.key1;
        var key2 = dest.key2;
        expect(definition.key1).to.have.been.calledOnce();
        expect(definition.key2).to.have.been.calledOnce();
      })
    ;

    it('should not recall the definition object value functions after first access', function() {
      var dest = {};
      var definition = {
        key1: sinon.spy(),
        key2: sinon.spy(),
      };
      BRest.$initOnDemand(dest, definition);
      var key11 = dest.key1;
      var key21 = dest.key2;
      var key12 = dest.key1;
      var key22 = dest.key2;
      expect(definition.key1).to.have.been.calledOnce();
      expect(definition.key2).to.have.been.calledOnce();
    });
  });

  it('should define $requestSenders', function() {
    staticModule(BRest);
    expect(BRest.$requestSenders).to.be.an('Object');
  });

  it('should define useRequestSender', function() {
    staticModule(BRest);
    expect(BRest.useRequestSender).to.be.a('Function');
  });

  describe('useRequestSender', function() {
    before(function() {
      staticModule(BRest);
    });

    it('should register the given function as the function to use to send requests ($sendRequest)', function() {
      var fn = sinon.spy();
      BRest.useRequestSender(fn);
      expect(BRest.$sendRequest).to.equal(fn);
    });

    it('should register the given function name of the $requestSenders registry to send requests ($sendRequest) if ' +
      'it exists', function() {
        BRest.$requestSenders.fn = sinon.spy();

        BRest.useRequestSender('fn');
        expect(BRest.$sendRequest).to.equal(BRest.$requestSenders.fn);
      })
    ;

    it('should throw an error if the given function name does not exist in the $requestSenders registry', function() {
      var thrown = function() {
        BRest.useRequestSender('fn2');
      };
      expect(thrown).to.throw('The request sender fn2 has not been registered as a request sender');
    });
  });

  it('should define registerRequestSender', function() {
    staticModule(BRest);
    expect(BRest.registerRequestSender).to.be.a('Function');
  });

  describe('registerRequestSender', function() {
    before(function() {
      staticModule(BRest);
    });

    it('should register the given function to the given name in the $requestSenders registry', function() {
      var fn = sinon.spy();
      BRest.registerRequestSender('fn', fn);
      expect(BRest.$requestSenders).to.have.property('fn').that.equal(fn);
    });

    it('should register it to send request if asked', function() {
      var fn = sinon.spy();
      BRest.registerRequestSender('fn', fn, true);
      expect(BRest.$sendRequest).to.equal(fn);
    });
  });

  it('should register by default XMLHttpRequest', function() {
    staticModule(BRest);
    expect(BRest.$requestSenders.XMLHttpRequest).to.be.a('Function');
  });

  it('should use by default XMLHttpRequest', function() {
    staticModule(BRest);
    expect(BRest.$sendRequest).to.equal(BRest.$requestSenders.XMLHttpRequest);
  });

  describe('XMLHttpRequest request sender', function() {
    var consoleError;
    var XMLHttpRequestSender;
    var requests;
    var respond = function() {
      requests[0].respond(200);
    };
    var respondInError = function() {
      requests[0].respond(404, {
        'Content-Type': 'text/plain',
        'Content-Length': 14
      },
      'Not found');
    };

    before(function() {
      staticModule(BRest);
      XMLHttpRequestSender = BRest.$requestSenders.XMLHttpRequest;
      XMLHttpRequest.onCreate = function(xhr) {
        // Temp fix, sinon v1.7 doesn't handle 'error' event
        xhr.addEventListener('readystatechange', function(e) {
          var xhr = e.target;
          // should set it on state 3 because loadend is sent on step 4 BEFORE this 'error' event
          if (xhr.readyState === 3 && (xhr.status < 200 || xhr.status > 299)) {
            xhr.dispatchEvent(new sinon.Event('error', false, false, xhr));
          }
        });
        requests.push(xhr);
      };
      console.debug = function() {};
      consoleError = console.error;
      console.error = function() {};
    });

    after(function() {
      console.error = consoleError;
    });

    beforeEach(function() {
      requests = [];
      sinon.spy(console, 'debug');
      sinon.spy(console, 'error');
      sinon.spy(BRest, '$urlValidator');
    });

    afterEach(function() {
      console.debug.restore();
      console.error.restore();
      BRest.$urlValidator.restore();
    });

    it('should throw an error if no XMLHttpRequest exist', function() {
      // jscs:disable
      // jshint ignore:start
      var tempXMLHttpRequest = XMLHttpRequest;
      XMLHttpRequest = null;
      var thrown =  function() {
        XMLHttpRequestSender();
      };
      expect(thrown).to.throw('Unable to use built-in sendRequest');
      XMLHttpRequest = tempXMLHttpRequest;
      // jscs:enable
      // jshint ignore:end
    });

    it('should return a Promise', function(done) {
      var promise = XMLHttpRequestSender();
      return expect(promise).to.be.an.instanceOf(Promise).notify(done.bind(null, undefined));
    });

    it('should be rejected if the given method is not a string', function() {
      return expect(XMLHttpRequestSender()).to.be.rejectedWith(/Wrong method/);
    });

    it('should be rejected if the given method is not a valid HTTP method', function() {
      return expect(XMLHttpRequestSender('action')).to.be.rejectedWith(/Wrong method/);
    });

    it('should be rejected if the given url is not a valid url (using $urlValidator)', function() {
      return XMLHttpRequestSender('get', 'not url').catch(function(err) {
        expect(err).to.be.an.instanceOf(Error);
        expect(err.message).to.match(/Invalid url/);
        expect(BRest.$urlValidator).to.have.been.calledOnce();
      });
    });

    it('should be resolved with the result of the XMLHttpRequest call', function() {
      var xhttp = XMLHttpRequestSender('get', 'http://localhost:9000');
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.properties({
        url: 'http://localhost:9000',
        method: 'get',
        requestHeaders: {},
        requestBody: null,
      });
      respond();
      return expect(xhttp).to.eventually.be.an.instanceOf(BRest.Request).that.
        deep.equal(BRest.Request.fromXMLHttpRequest(requests[0]));
    });

    it('should be resolved with the result of the XMLHttpRequest call even if the method is in upper case', function() {
      var xhttp = XMLHttpRequestSender('GET', 'http://localhost:9000');
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.property('method').that.equal('get');
      respond();
      return expect(xhttp).to.eventually.be.an.instanceOf(BRest.Request).that.
        deep.equal(BRest.Request.fromXMLHttpRequest(requests[0]));
    });

    it('should log some data on verbose mode if all is ok', function() {
      var xhttp = XMLHttpRequestSender('get', 'http://localhost:9000', { verbose: true });
      respond();
      return xhttp.then(function() {
        expect(console.debug).to.have.been.calledTwice();
      });
    });

    it('should be rejected with an error containing the result of the XMLHttpRequest call', function() {
      var xhttp = XMLHttpRequestSender('get', 'http://localhost:9000');
      expect(requests).to.have.lengthOf(1);
      respondInError();
      return expect(xhttp).to.be.rejected().and.that.eventually.have.property('xhttp').
        which.is.an.instanceOf(XMLHttpRequest).that.equal(requests[0]);
    });

    it('should log some data on verbose mode if all is ok', function() {
      var xhttp = XMLHttpRequestSender('get', 'http://localhost:9000', { verbose: true });
      respondInError();
      return xhttp.catch(function() {
        expect(console.debug).to.have.been.calledOnce();
        expect(console.error).to.have.been.calledOnce();
      });
    });

    it('should send the body if given, is a string and POST method', function() {
      var body = 'plain text';
      var xhttp = XMLHttpRequestSender('post', 'http://localhost:9000', {
        body: body,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.property('requestBody').that.equal(body);
    });

    // probably won't work but should be possible according to HTTP norm
    it.skip('should send the body if given and GET method', function() {
      var body = 'plain text';
      var xhttp = XMLHttpRequestSender('get', 'http://localhost:9000', {
        body: body,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.property('requestBody').that.equal(body);
    });

    it('should send the body if it is given and an object, using JSON.stringify', function() {
      var body = {value: 'plain text'};
      var xhttp = XMLHttpRequestSender('post', 'http://localhost:9000', {
        body: body,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.property('requestBody').that.equal('{"value":"plain text"}');
    });

    it('should reject an error if stringification fails', function() {
      var body = {};
      body.a = {b: body};
      return expect(XMLHttpRequestSender('post', 'http://localhost:9000', {
        body: body,
      })).to.be.rejectedWith(/Unable to stringify the body/);
    });

    it('should send the headers in lowered kebab case if given', function() {
      var headers = {
        'Content-type': 'text/plain',
        'CamelPerformed': 'myTests',
        'testFirstLowerCase': 'test',
      };
      var xhttp = XMLHttpRequestSender('get', 'http://localhost:9000', {
        headers: headers,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.property('requestHeaders').that.deep.equal({
        'content-type': 'text/plain',
        'camel-performed': 'myTests',
        'test-first-lower-case': 'test',
      });
    });

    it('should update the content-type header if the body is an object', function() {
      var headers = {
        'Content-type': 'text/plain',
      };
      var body = {value: 'test'};
      var xhttp = XMLHttpRequestSender('post', 'http://localhost:9000', {
        headers: headers,
        body: body,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0].requestHeaders).to.have.property('content-type').that.match(/application\/plain\+json/);
    });

    it('should set the content-type header if the body is an object', function() {
      var body = {value: 'test'};
      var xhttp = XMLHttpRequestSender('post', 'http://localhost:9000', {
        body: body,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0].requestHeaders).to.have.property('content-type').that.match(/application\/json/);
    });

    it('should enhance the url with the given params', function() {
      var params = {
        value: 'test',
        'to escape': '<checks if it+works é.è>',
      };
      var xhttp = XMLHttpRequestSender('post', 'http://localhost:9000', {
        params: params,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.property('url').that.equal('http://localhost:9000/?value=test&' +
        'to%20escape=%3Cchecks%20if%20it%2Bworks%20%C3%A9.%C3%A8%3E');
    });

    it('should priorize the given params to the URL ones', function() {
      var params = {
        value: 'test',
        'value2': 'test2',
      };
      var xhttp = XMLHttpRequestSender('post', 'http://localhost:9000?value=noTest', {
        params: params,
      });
      expect(requests).to.have.lengthOf(1);
      expect(requests[0]).to.have.property('url').that.equal('http://localhost:9000/?value=test&value2=test2');
    });
  });

  it('should define $urlValidator', function() {
    staticModule(BRest);
    expect(BRest.$urlValidator).to.be.a('Function');
  });

  describe('$urlValidator', function() {
    before(function() {
      staticModule(BRest);
    });

    it('should return false if the param is not a string', function() {
      expect(BRest.$urlValidator({})).to.be.false();
    });

    it('should return false if the param is not a valid url (no protocol)', function() {
      expect(BRest.$urlValidator('not an url')).to.be.false();
    });

    it('should return true if the param is a valid url', function() {
      expect(BRest.$urlValidator('ssh://test@test')).to.be.true();
    });
  });
});
