'use strict';

var chai = require('chai');
var sinon = require('sinon');
var expect = chai.expect;
var _ = require('lodash');

describe('module Request', function() {
  var RequestModule = require('./request');
  var BRest = {};

  it('should be a function', function() {
    expect(RequestModule).to.be.a('Function');
  });

  it('should return nothing', function() {
    expect(RequestModule(BRest)).to.be.undefined();
  });

  it('should define Request', function() {
    RequestModule(BRest);
    expect(BRest.Request).to.be.a('Function');
  });

  describe('Request', function() {
    var Request;

    before(function() {
      RequestModule(BRest);
      Request = BRest.Request;
    });
  });
});
