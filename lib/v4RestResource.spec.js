'use strict';

var chai = require('chai');
var sinon = require('sinon');
var expect = chai.expect;
var _ = require('lodash');
var Promise = require('bluebird');
var xhr = require('../utils/requestForXhrStub')(require('../utils/xhrStub'));
var fixture = _.partial(require('../utils/test'), 'v4');

describe('module v4RestResource', function() {
  var RestResourceModule = require('./v4RestResource');
  var StaticModule = require('./static');
  var AbstractRestResourceModule = require('./restResource');
  var RequestModule = require('./request');
  var BRest = {};

  before(function() {
    StaticModule(BRest);
    AbstractRestResourceModule(BRest);
    RequestModule(BRest);
    xhr.setRequest(BRest.Request);
  });

  beforeEach(function() {
    sinon.spy(BRest, '$initOnDemand');
  });

  afterEach(function() {
    BRest.$initOnDemand.restore();
  });

  it('should be a function', function() {
    expect(RestResourceModule).to.be.a('Function');
  });

  it('should return nothing', function() {
    expect(RestResourceModule(BRest)).to.be.undefined();
  });

  it('should define v4.RestResource', function() {
    RestResourceModule(BRest);
    expect(BRest.v4.RestResource).to.be.a('Function');
  });

  describe('RestResource', function() {
    var RestResource;

    before(function() {
      RestResourceModule(BRest);
      RestResource = BRest.v4.RestResource;

      xhr.defaults.setHeader('server', 'ApiBenchmark');
    });

    beforeEach(function() {
      xhr.reset();
    });

    it('should be an instanceof BRest.RestResource', function() {
      expect(new RestResource(xhr(fixture('summary')()))).to.be.instanceOf(BRest.RestResource);
    });

    it('should access to BRest.RestResource via super_ property', function() {
      expect(RestResource).to.have.property('super_').that.equal(BRest.RestResource);
    });

    it('should initialize on demand $apiVersion to v4', function() {
      var restResource = new RestResource(xhr(fixture('summary')()));
      expect(restResource).to.have.property('$apiVersion').that.equal('v4');
      expect(BRest.$initOnDemand).to.have.been.called();
    });

    describe('on summary API', function() {
      before(function() {
        xhr.setDefaultsObject(fixture('summary')());
      });

      describe('$self', function() {
        it('should be initialized on demand to Content-Location Header if exists', function() {
          var contentLocationHeader = 'url';
          xhr.setHeader('Content-Location', contentLocationHeader);
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$self').that.equal(contentLocationHeader);
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should be initialized on demand to responseURL otherwise', function() {
          xhr.unsetHeader('Content-Location');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$self').that.equal(xhr.get('requestUrl'));
          expect(BRest.$initOnDemand).to.have.been.called();
        });
      });

      describe('$version', function() {
        it('should be initialized on demand to ETag Header if exists', function() {
          var ETagHeader = 'version';
          xhr.setHeader('ETag', ETagHeader);
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$version').that.equal(ETagHeader);
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should be initialized on demand to 0 otherwise', function() {
          xhr.unsetHeader('ETag');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$version').that.equal(0);
          expect(BRest.$initOnDemand).to.have.been.called();
        });
      });

      describe.skip('$type', function() {
        it('should be initialized on demand to type if the content-type Header has a type (removing JSON type)',
          function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource).to.have.property('$type').that.equal('vnd.api-benchmark.summary');
            expect(BRest.$initOnDemand).to.have.been.called();
          })
        ;

        it('should be initialized on demand to type if the content-type Header has a type (removing JSON type) even ' +
          'if it is inversed', function() {
            xhr.setHeader('Content-Type', 'application/json+vnd.api-benchmark.summary');
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource).to.have.property('$type').that.equal('vnd.api-benchmark.summary');
            expect(BRest.$initOnDemand).to.have.been.called();
          })
        ;

        it('should throw an error if more than one type is found', function() {
          xhr.setHeader('Content-Type', 'application/json+other+vnd.api-benchmark.summary');
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            var type = restResource.$type;
          };
          expect(thrown).to.throw('Multiple types found');
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should be initialized on demand to 0 otherwise', function() {
          xhr.setHeader('Content-Type', 'application/json');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$type').that.is.undefined();
          expect(BRest.$initOnDemand).to.have.been.called();
        });
      });

      describe('content', function() {
        it('should be empty if no responseText', function() {
          xhr.setBody(undefined);
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.be.empty();
        });

        it('should throw an error if responseText is not JSON and no parser has been given in options', function() {
          xhr
            .setHeader('Content-Type', 'text/plain')
            .setBody('Hello world!')
          ;
          var thrown = function() {
            new RestResource(xhr.xhttp);
          };
          expect(thrown).to.throw('Non-JSON response need a parser');

          thrown = function() {
            new RestResource(xhr.xhttp, {});
          };
          expect(thrown).to.throw('Non-JSON response need a parser');
        });

        it('should use a parser if a parser has been given in options', function() {
          xhr.setBody('Hello world!');
          var parser = sinon.stub().returns({});
          var restResource = new RestResource(xhr.xhttp, {
            parser: parser,
          });
          expect(restResource).to.be.empty();
          expect(parser).to.have.been.called();
        });

        it('should use default JSON.parse if no parser has been given in options', function() {
          xhr.setBody('{"test":"test"}');
          var restResource = new RestResource(xhr.xhttp, {});
          expect(restResource).to.have.properties({ test: 'test' });
        });

        it('should throw an error if parsing fails', function() {
          xhr.setBody('{');
          var thrown = function() {
            new RestResource(xhr.xhttp);
          };
          expect(thrown).to.throw('Body parsing failed');
        });

        it('should throw an error if the parsed responseText is not a JSON object', function() {
          xhr.setBody('Hello world!');
          var thrown = function() {
            new RestResource(xhr.xhttp, {
              parser: sinon.stub().returns(''),
            });
          };
          expect(thrown).to.throw('Body parsing failed');
        });
      });

      // TODO type handling
      it.skip('should initialize on demand $schema to undefined', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$schema').that.is.undefined();
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      it('should initialize on demand a $builder', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$builder').that.is.a('Function');
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      describe('$links', function() {
        it('should be initialized on demand to an empty object if no Link Header', function() {
            xhr.unsetHeader('Link');
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource).to.have.property('$links').that.deep.equal({});
            expect(BRest.$initOnDemand).to.have.been.called();
          })
        ;

        it('should be initialized on demand to non-numeric keys links (numeric keys are collection links) ' +
          'from Link Header', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource).to.have.property('$links').that.deep.equal({
              ingredients: 'http://localhost:9000/ingredients',
              recipes: 'http://localhost:9000/recipes',
            });
            expect(BRest.$initOnDemand).to.have.been.called();
          })
        ;
      });

      describe('$generalOptions', function() {
        it('should be initialized on demand to an empty object if no options given', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource).to.have.property('$generalOptions').that.deep.equal({});
            expect(BRest.$initOnDemand).to.have.been.called();
          })
        ;

        it('should be initialized on demand to the given options', function() {
            var options = {};
            var restResource = new RestResource(xhr.xhttp, options);
            expect(restResource).to.have.property('$generalOptions').that.equal(options);
            expect(BRest.$initOnDemand).to.have.been.called();
          })
        ;
      });

      it('should initialize on demand $isCollection to false (not a collection type (collection))', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$isCollection').that.is.false();
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      it('should initialize on demand $collection to an empty array', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$collection').that.deep.equal([]);
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      it('should initialize on demand $hashMap to an empty object', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$hashMap').that.deep.equal({});
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      it('should initialize on demand $range to an empty object (no Content-Range header)', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$range').that.deep.equal({});
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      it('should initialize on demand $navigation to an empty object (no Content-Range header)', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$navigation');
        expect(restResource.$navigation).to.deep.equal({});
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      describe('follow', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            return Promise.resolve(xhr(fixture('recipes', '0-9')()));
          };
        });

        beforeEach(function() {
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('follow').that.is.a('Function');
        });

        it('should throw an error if the link to follow does not exist', function() {
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.follow('noLink');
          };
          expect(thrown).to.throw('Not a link');
        });

        it('should return a Promise that returns the new resource corresponding to the followed link, using ' +
          'BRest.$sendRequest', function() {
          var restResource = new RestResource(xhr.xhttp);
          var follow = restResource.follow('recipes');
          expect(follow).to.be.an.instanceOf(Promise);
          return follow.then(function(res) {
            expect(BRest.$sendRequest).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes');
            expect(res).to.be.an.instanceOf(RestResource);
          });
        });

        it('should pass the options to the request sender', function() {
          var restResource = new RestResource(xhr.xhttp);
          var options = { test: 'test' };
          return restResource.follow('recipes', options).then(function(res) {
            expect(BRest.$sendRequest.lastCall.args[2]).to.have.properties(options);
          });
        });
      });

      describe('toObject', function() {
        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('toObject').that.is.a('Function');
        });

        it('should return an object', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.toObject()).to.be.an('Object');
        });

        it('should return an object that contain all the resource, especially hidden attributes', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.toObject()).to.have.properties({
            $apiVersion: restResource.$apiVersion,
            $self: restResource.$self,
            $version: restResource.$version,
            $type: restResource.$type,
            $schema: restResource.$schema,
            $links: restResource.$links,
            $isCollection: restResource.$isCollection,
            $generalOptions: restResource.$generalOptions,
            body: {},
          });
        });
      });

      describe('toString', function() {
        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('toString').that.is.a('Function');
        });

        it('should return a string', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.toString()).to.be.a('String');
        });

        it('should return a string that contain all the resource, especially hidden attributes, with a 2-spaces ' +
          'indentation', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource.toString()).to.equal(JSON.stringify(restResource.toObject(), null, 2));
          })
        ;

        it('should return a string that contain all the resource, especially hidden attributes, with a  no ' +
          'indentation', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource.toString(true)).to.equal(JSON.stringify(restResource.toObject()));
          })
        ;
      });
    });

    describe('on collection API', function() {
      before(function() {
        xhr.setDefaultsObject(fixture('recipes', '0-9')());
      });

      it('should initialize on demand $links to non-numeric keys links (numeric keys are collection links) ' +
        'from Link Header', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$links').that.deep.equal({});
          expect(BRest.$initOnDemand).to.have.been.called();
        })
      ;

      it('should initialize on demand $isCollection to true', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$isCollection').that.is.true();
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      it('should initialize on demand $collection to an array', function() {
        var restResource = new RestResource(xhr.xhttp);
        expect(restResource).to.have.property('$collection').that.deep.equal([
            'http://localhost:9000/recipes/mousse-chocolat',
            'http://localhost:9000/recipes/tarte-citron-meringuee',
            'http://localhost:9000/recipes/tarte-pomme',
            'http://localhost:9000/recipes/sorbet-fraise',
            'http://localhost:9000/recipes/sable-cannelle',
            'http://localhost:9000/recipes/millefeuille',
            'http://localhost:9000/recipes/pate-amande',
            'http://localhost:9000/recipes/galette-rois',
            'http://localhost:9000/recipes/ile-flottante',
            'http://localhost:9000/recipes/foret-noire',
        ]);
        expect(BRest.$initOnDemand).to.have.been.called();
      });

      describe('$hashMap', function() {
        it('should be initialized on demand to an object starting at start index (0 by default)', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$hashMap').that.deep.equal({
            '0': 'http://localhost:9000/recipes/mousse-chocolat',
            '1': 'http://localhost:9000/recipes/tarte-citron-meringuee',
            '2': 'http://localhost:9000/recipes/tarte-pomme',
            '3': 'http://localhost:9000/recipes/sorbet-fraise',
            '4': 'http://localhost:9000/recipes/sable-cannelle',
            '5': 'http://localhost:9000/recipes/millefeuille',
            '6': 'http://localhost:9000/recipes/pate-amande',
            '7': 'http://localhost:9000/recipes/galette-rois',
            '8': 'http://localhost:9000/recipes/ile-flottante',
            '9': 'http://localhost:9000/recipes/foret-noire',
          });
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should be initialized on demand to an object starting at a start index other than 0', function() {
          xhr.setParamsObject(fixture('recipes', '10-19')());
          var restResource = new RestResource(xhr.xhttp);
          expect(Object.keys(restResource.$hashMap)).to.deep.equal(
            ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19']
          );
          expect(BRest.$initOnDemand).to.have.been.called();
        });
      });

      describe('$range', function() {
        it('should be initialized on demand to an object', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$range').that.is.an('Object');
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should contain end, type and total (from Range Header)', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$range).to.have.properties({
            type: 'recipe',
            total: 31,
            end: 9,
          });
        });

        it('should initialize $range.start to range header start if exist', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$range).to.have.properties({
            start: 0,
          });
        });

        it('should initialize $range.start to $range.end otherwise', function() {
          xhr.setHeader('Content-Range', 'recipe 1/41');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$range).to.have.properties({
            end: 1,
            start: 1,
          });
        });

        it('should contain size that is the number of elements on the page, based on $range data', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$range).to.have.property('size').that.equal(10);

          xhr.setHeader('Content-Range', 'recipe 1/41');
          var secondRestResource = new RestResource(xhr.xhttp);
          expect(secondRestResource.$range).to.have.property('size').that.equal(1);
        });

        it.skip('should contain step that is the requested number of element for each page and same as size since no ' +
          'navigation', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$range).to.have.property('step').that.equal(10);
        });
      });

      describe('$navigation', function() {
        it('should be initialized on demand to an object', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$navigation').that.is.a('Object');
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should throw an error if the Content-Range header is invalid', function() {
          xhr.setHeader('Content-Range', 'ingredient 0-NaN/undefined');
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            var navigation = restResource.$navigation;
          };
          expect(thrown).to.throw('Invalid range header ingredient 0-NaN/undefined');
        });

        it('should have a current property corresponding to the current collection page', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.have.property('current').that.equal('recipe 0-9');
        });

        it('should have a first property if start is not 0', function() {
          xhr.setHeader('Content-Range', 'recipe 1-2/10');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.have.property('first').that.equal('recipe 0-1');
        });

        it('should have no first property if start is 0', function() {
          xhr.setHeader('Content-Range', 'recipe 0/10');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.not.have.property('first');
        });

        it('should have a last property if the last record is not returned', function() {
          xhr.setHeader('Content-Range', 'recipe 8-9/12');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.have.property('last').that.equal('recipe 10-11');
        });

        it('should have no last property if the last record is returned', function() {
          xhr.setHeader('Content-Range', 'recipe 8-9/10');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.not.have.property('last');
        });

        it('should have a prev property if the first record is not returned', function() {
          xhr.setHeader('Content-Range', 'recipe 8-9/12');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.have.property('prev').that.equal('recipe 6-7');
        });

        it('should have no prev property if the first record is returned', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.not.have.property('prev');
        });

        it('should have a next property with different size if asked and the last record is not returned', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.have.property('next').that.equal('recipe 10-19');
        });

        it('should have no next property if the last record is returned', function() {
          xhr.setHeader('Content-Range', 'recipe 10-11/12');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.not.have.property('next');
        });

        describe('step', function() {
          it('should be defined as $navigation property', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource.$navigation).to.have.property('step').that.is.a('Function');
          });

          it('should return the first property with different size', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource.$navigation.step(2)).to.equal('recipe 0-1');
          });
        });

        it('should set only one number on different properties if size is 1', function() {
          xhr.setHeader('Content-Range', 'recipe 5/10');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.have.properties({
            first: 'recipe 0',
            last: 'recipe 9',
            prev: 'recipe 4',
            next: 'recipe 6',
            current: 'recipe 5',
          });
        });

        it('should set only one number if it is the last one even if requested more than one', function() {
          xhr.setHeader('Content-Range', 'recipe 0/1');
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.$navigation).to.have.property('current').that.equal('recipe 0');
        });
      });

      describe('getStep', function() {
        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('setStep').that.is.a('Function');
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it.skip('should return this.$range.step', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource.getStep()).to.equal(10);
        });
      });

      describe('setStep', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            return Promise.resolve(xhr(fixture('recipes/0-4')()));
          };
        });

        beforeEach(function() {
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('setStep').that.is.a('Function');
        });

        it('should throw an error if the current resource is not a collection', function() {
          xhr.setHeader('Content-Type', 'application/vnd.api-benchmark.summary+json');
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.setStep();
          };
          expect(thrown).to.throw('Not a collection');
        });

        it('should throw an error if the requested size is not a valid number', function() {
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.setStep('test');
          };
          expect(thrown).to.throw('Not a valid step');
        });

        it('should return a Promise that returns the new resource corresponding to the wished page of the ' +
          'collection', function() {
            var restResource = new RestResource(xhr.xhttp);
            BRest.$sendRequest.reset();

            return restResource.setStep(5).then(function() {
              expect(BRest.$sendRequest.lastCall).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
                headers: {
                  range: 'recipe 0-4',
                },
              });
            });
          })
        ;

        it('should pass the options to the request sender', function() {
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();

          var options = { test: 'test' };
          return restResource.setStep(5, options).then(function() {
            expect(BRest.$sendRequest.lastCall.args[2]).to.have.properties(options);
          });
        });
      });

      describe('navigate (next, last, first, prev)', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            return Promise.resolve(xhr(fixture('recipes/10-19')()));
          };
          xhr.setDefaultsObject(fixture('recipes/20-29.js')());
        });

        after(function() {
          xhr.setDefaultsObject(fixture('recipes/0-9.js')());
        });

        beforeEach(function() {
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        _.forEach(['next', 'last', 'first', 'prev'], function(fnName) {
          it('should define ' + fnName + ' as a function', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource).to.have.property(fnName).that.is.a('Function');
          });

          it('should throw an error in ' + fnName + ' if the current resource is not a collection', function() {
            xhr.setHeader('Content-Type', 'application/vnd.api-benchmark.summary+json');
            var restResource = new RestResource(xhr.xhttp);
            var thrown = function() {
              restResource[fnName]();
            };
            expect(thrown).to.throw('Not a collection');
          });

          it('should return a Promise in ' + fnName + ' resolved with the current resource if the navigation way is ' +
            'impossible', function() {
              xhr.setHeader('Content-Range', 'recipe 0/1');
              var restResource = new RestResource(xhr.xhttp);
              var res = restResource[fnName]();
              expect(res).to.be.an.instanceof(Promise);
              return res.then(function(result) {
                expect(result).to.equal(restResource);
              });
            })
          ;

          it('should pass the options in ' + fnName + ' to the request sender', function() {
            var restResource = new RestResource(xhr.xhttp);
            BRest.$sendRequest.reset();

            var options = { test: 'test' };
            return restResource[fnName](options).then(function() {
              expect(BRest.$sendRequest.lastCall.args[2]).to.have.properties(options);
            });
          });
        });

        it('should return a Promise in first that returns the new resource corresponding to the wished page of the ' +
          'collection',
          function() {
            var restResource = new RestResource(xhr.xhttp);
            BRest.$sendRequest.reset();

            return restResource.first().then(function() {
              expect(BRest.$sendRequest.lastCall).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
                headers: {
                  range: 'recipe 0-9',
                },
              });
            });
          })
        ;

        it('should return a Promise in last that returns the new resource corresponding to the wished page of the ' +
          'collection',
          function() {
            var restResource = new RestResource(xhr.xhttp);
            BRest.$sendRequest.reset();

            return restResource.last().then(function() {
              expect(BRest.$sendRequest.lastCall).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
                headers: {
                  range: 'recipe 30',
                },
              });
            });
          })
        ;

        it('should return a Promise in next that returns the new resource corresponding to the wished page of the ' +
          'collection',
          function() {
            var restResource = new RestResource(xhr.xhttp);
            BRest.$sendRequest.reset();

            return restResource.next().then(function() {
              expect(BRest.$sendRequest.lastCall).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
                headers: {
                  range: 'recipe 30',
                },
              });
            });
          })
        ;

        it('should return a Promise in prev that returns the new resource corresponding to the wished page of the ' +
          'collection',
          function() {
            var restResource = new RestResource(xhr.xhttp);
            BRest.$sendRequest.reset();

            return restResource.prev().then(function() {
              expect(BRest.$sendRequest.lastCall).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
                headers: {
                  range: 'recipe 10-19',
                },
              });
            });
          })
        ;

        it('should define previous as prev', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('previous').that.is.a('Function').that.equal(restResource.prev);
        });
      });

      describe('expand', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            if (!url.match(/\/([\w-]+)$/)) {
              throw new Error('Invalid URL ' + url);
            }
            var id = RegExp.$1;
            return Promise.resolve(xhr(fixture('recipe')(id)));
          };
        });

        beforeEach(function() {
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('expand').that.is.a('Function');
        });

        it('should throw an error if the current resource is not a collection', function() {
          xhr.setHeader('Content-Type', 'application/vnd.api-benchmark.summary+json');
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.expand();
          };
          expect(thrown).to.throw('Not a collection');
        });

        it('should return a Promise resolved with an array, one result for each page resource', function() {
          var restResource = new RestResource(xhr.xhttp);
          var res = restResource.expand();
          expect(res).to.be.an.instanceof(Promise);
          return res.then(function(result) {
            expect(result).to.be.an('Array').that.have.length(restResource.$range.size);
          });
        });

        it('should call each page resource', function() {
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();
          restResource.expand().then(function(res) {
            expect(BRest.$sendRequest).to.have.been.called();
            expect(BRest.$sendRequest.callCount).to.equal(restResource.$range.size);
            _.forEach(res, function(expanded, i) {
              expect(BRest.$sendRequest.getCall(i)).to.have.been.calledWith('HEAD', restResource.$collection[i]);
              expect(expanded).to.be.an.instanceOf(RestResource);
            });
          });
        });

        it('should pass the options to the request sender', function() {
          var restResource = new RestResource(xhr.xhttp);
          var options = { test: 'test' };
          return restResource.expand(options).then(function(res) {
            _.forEach(res, function(expanded, i) {
              expect(BRest.$sendRequest.getCall(i).args[2]).to.have.properties(options);
            });
          });
        });
      });

      describe('getAll', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            return Promise.resolve(xhr());
          };
        });

        beforeEach(function() {
          xhr.setBatch([
            fixture('recipes', '10-19')(),
            fixture('recipes', '20-29.js')(),
            fixture('recipes', '30')(),
          ]);
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('getAll').that.is.a('Function');
        });

        it('should throw an error if the current resource is not a collection', function() {
          xhr.setHeader('Content-Type', 'application/vnd.api-benchmark.summary+json');
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.getAll();
          };
          expect(thrown).to.throw('Not a collection');
        });

        it('should return a Promise resolved with an hashMap', function() {
          var restResource = new RestResource(xhr.xhttp);
          var res = restResource.getAll();
          expect(res).to.be.an.instanceof(Promise);
          return res.then(function(result) {
            expect(result).to.be.an('Object');
          });
        });

        it('should have called all missing pages of the collection',function() {
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();

          return restResource.getAll().then(function(res) {
            expect(BRest.$sendRequest.callCount).to.equal(_.ceil(31 / 10) - 1);
            expect(BRest.$sendRequest.getCall(0)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 10-19',
              },
            });
            expect(BRest.$sendRequest.getCall(1)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 20-29',
              },
            });
            expect(BRest.$sendRequest.getCall(2)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 30',
              },
            });
          });
        });

        it('should return a Promise resolved with an hashMap of the collection elements links',function() {
          var restResource = new RestResource(xhr.xhttp);
          return restResource.getAll().then(function(res) {
            expect(_.size(res)).to.equal(31);
            for (var i = 0; i < 31; ++i) {
              expect(res[i]).to.match(/^http:\/\/localhost:9000\/recipes\/[\w-]+\/?$/);
            }
          });
        });

        it('should pass the options to the request sender', function() {
          var restResource = new RestResource(xhr.xhttp);
          var options = { test: 'test' };
          return restResource.getAll(options).then(function(res) {
            for (var i = 0; i < 3; ++i) {
              expect(BRest.$sendRequest.getCall(i).args[2]).to.have.properties(options);
            }
          });
        });
      });

      describe('getAllByPage', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            return Promise.resolve(xhr());
          };
        });

        beforeEach(function() {
          xhr.setBatch([
            fixture('recipes', '10-19')(),
            fixture('recipes', '20-29.js')(),
            fixture('recipes', '30')(),
          ]);
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('getAllByPage').that.is.a('Function');
        });

        it('should throw an error if the current resource is not a collection', function() {
          xhr.setHeader('Content-Type', 'application/vnd.api-benchmark.summary+json');
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.getAllByPage();
          };
          expect(thrown).to.throw('Not a collection');
        });

        it('should return a Promise resolved with an array of pages', function() {
          var restResource = new RestResource(xhr.xhttp);
          var res = restResource.getAllByPage();
          expect(res).to.be.an.instanceof(Promise);
          return res.then(function(result) {
            expect(result).to.be.an('Array');
          });
        });

        it('should have called all missing pages of the collection', function() {
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();
          return restResource.getAllByPage().then(function(res) {
            expect(BRest.$sendRequest.callCount).to.equal(3);
            expect(BRest.$sendRequest.getCall(0)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 10-19',
              },
            });
            expect(BRest.$sendRequest.getCall(1)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 20-29',
              },
            });
            expect(BRest.$sendRequest.getCall(2)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 30',
              },
            });
          });
        });

        it('should return a Promise resolved with an array containing collection resources', function() {
          var restResource = new RestResource(xhr.xhttp);
          return restResource.getAllByPage().then(function(res) {
            expect(_.size(res)).to.equal(_.ceil(31 / 10)); // number of pages (10 is the default size in these examples)
            for (var i = 0; i < _.ceil(31 / 10); ++i) {
              expect(res[i]).to.be.an.instanceof(RestResource);
            }
          });
        });

        it('should paginate to the given size if exists', function() {
          xhr.clearBatch();
          xhr.setBatch([
            fixture('recipes', '0-4')(),
            fixture('recipes', '5-9')(),
            fixture('recipes', '10-14')(),
            fixture('recipes', '15-19')(),
            fixture('recipes', '20-24')(),
            fixture('recipes', '25-29')(),
            fixture('recipes', '30')(),
          ]);
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();

          return restResource.getAllByPage(5).then(function(res) {
            expect(BRest.$sendRequest.callCount).to.equal(_.ceil(31 / 5));
            expect(BRest.$sendRequest.getCall(0)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 0-4',
              },
            });
            expect(BRest.$sendRequest.getCall(1)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 5-9',
              },
            });
            expect(BRest.$sendRequest.getCall(2)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 10-14',
              },
            });
            expect(BRest.$sendRequest.getCall(3)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 15-19',
              },
            });
            expect(BRest.$sendRequest.getCall(4)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 20-24',
              },
            });
            expect(BRest.$sendRequest.getCall(5)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 25-29',
              },
            });
            expect(BRest.$sendRequest.getCall(6)).to.have.been.calledWith('HEAD', 'http://localhost:9000/recipes', {
              headers: {
                range: 'recipe 30',
              },
            });
          });
        });

        it('should pass the options to the request sender', function() {
          xhr.clearBatch();
          xhr.setBatch([
            fixture('recipes', '0-4')(),
            fixture('recipes', '5-9')(),
            fixture('recipes', '10-14')(),
            fixture('recipes', '15-19')(),
            fixture('recipes', '20-24')(),
            fixture('recipes', '25-29')(),
            fixture('recipes', '30')(),
          ]);
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();

          var options = { test: 'test' };
          return restResource.getAllByPage(5, options).then(function(res) {
            for (var i = 0; i < _.ceil(31 / 5); ++i) {
              expect(BRest.$sendRequest.getCall(i).args[2]).to.have.properties(options);
            }
          });
        });

        it('should pass the options to the request sender even if no size', function() {
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();

          var options = { test: 'test' };
          return restResource.getAllByPage(options).then(function(res) {
            for (var i = 0; i < _.ceil(31 / 10) - 1; ++i) { // first recipe do not make an HTTP call
              expect(BRest.$sendRequest.getCall(i).args[2]).to.have.properties(options);
            }
          });
        });
      });

      describe('toObject', function() {
        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('toObject').that.is.a('Function');
        });

        it('should return an object that contain all the resource, especially hidden attributes, with ' +
          'collection-only data', function() {
            var restResource = new RestResource(xhr.xhttp);
            expect(restResource.toObject()).to.deep.equal({
              $apiVersion: restResource.$apiVersion,
              $self: restResource.$self,
              $version: restResource.$version,
              $type: restResource.$type,
              $schema: restResource.$schema,
              $links: restResource.$links,
              $isCollection: restResource.$isCollection,
              $generalOptions: restResource.$generalOptions,
              body: {
                version: '1.0',
              },
              $collection: restResource.$collection,
              $hashMap: restResource.$hashMap,
              $navigation: restResource.$navigation,
              $range: restResource.$range,
            });
          })
        ;
      });

      describe('get', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            if (!url.match(/\/([\w-]+)$/)) {
              throw new Error('Invalid URL ' + url);
            }
            var id = RegExp.$1;
            return Promise.resolve(xhr(fixture('recipe')(id)));
          };
        });

        beforeEach(function() {
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('get').that.is.a('Function');
        });

        it('should throw an error if the current resource is not a collection', function() {
          xhr.setHeader('Content-Type', 'application/vnd.api-benchmark.summary+json');
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.get();
          };
          expect(thrown).to.throw('Not a collection');
        });

        it('should throw an error if the asked entity does not exist', function() {
          var restResource = new RestResource(xhr.xhttp);
          var thrown = function() {
            restResource.get(128);
          };
          expect(thrown).to.throw('No entity found');
        });

        it('should call the entity resource', function() {
          var restResource = new RestResource(xhr.xhttp);
          BRest.$sendRequest.reset();
          restResource.get(1).then(function(res) {
            expect(BRest.$sendRequest).to.have.been.calledWith('GET',
              'http://localhost:9000/recipes/tarte-citron-meringuee');
          });
        });

        it('should return a Promise resolved the requested entity', function() {
          var restResource = new RestResource(xhr.xhttp);
          var res = restResource.get(1);
          expect(res).to.be.an.instanceof(Promise);
          return res.then(function(result) {
            expect(result).to.be.an.instanceOf(RestResource);
            expect(result.$self).to.equal('http://localhost:9000/recipes/tarte-citron-meringuee');
          });
        });

        it('should return a Promise resolved the requested entity even if the parameter is a string', function() {
          var restResource = new RestResource(xhr.xhttp);
          var res = restResource.get('1');
          expect(res).to.be.an.instanceof(Promise);
          return res.then(function(result) {
            expect(result).to.be.an.instanceOf(RestResource);
            expect(result.$self).to.equal('http://localhost:9000/recipes/tarte-citron-meringuee');
          });
        });

        it('should pass the options to the request sender', function() {
          var restResource = new RestResource(xhr.xhttp);
          var options = { test: 'test' };
          return restResource.get(1, options).then(function(res) {
            expect(BRest.$sendRequest.getCall(0).args[2]).to.have.properties(options);
          });
        });
      });

      describe('expandAll', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            if (!url.match(/\/([\w-]+)$/)) {
              return Promise.resolve(xhr());
            }
            var id = RegExp.$1;
            return Promise.resolve(xhr(fixture('recipe')(id)));
          };
        });

        beforeEach(function() {
          xhr.setBatch([
            fixture('recipes', '10-19')(),
            fixture('recipes', '20-29.js')(),
            fixture('recipes', '30')(),
          ]);
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('expandAll').that.is.a('Function');
        });

        it('should call getAll', function() {
          var options = {test: 'test'};
          var restResource = new RestResource(xhr.xhttp);

          sinon.spy(restResource, 'getAll');

          restResource.expandAll(options);
          expect(restResource.getAll).to.have.been.calledOnce();
          expect(restResource.getAll).to.have.been.calledWith(options);
        });

        it('should call each resource of getAll (with options if any) and return a promise containing an hashMap of ' +
          'each resource result', function() {
            var options = {test: 'test'};
            var restResource = new RestResource(xhr.xhttp);

            var res = restResource.expandAll(options);
            expect(res).to.be.an.instanceOf(Promise);

            var nbOfElements = 31;
            var nbOfGetAllCalls = 3;

            return res.then(function(res) {
              expect(BRest.$sendRequest.callCount).to.equal(nbOfGetAllCalls + nbOfElements);
              expect(res).to.be.an('object');
              expect(_.size(res)).to.equal(31);
              expect(_.every(res, function(result) {
                return (result instanceof RestResource);
              }));
            });
          })
        ;
      });

      describe('expandAllByPage', function() {
        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            if (!url.match(/\/([\w-]+)$/)) {
              return Promise.resolve(xhr());
            }
            var id = RegExp.$1;
            return Promise.resolve(xhr(fixture('recipe')(id)));
          };
        });

        beforeEach(function() {
          xhr.setBatch([
            fixture('recipes', '10-19')(),
            fixture('recipes', '20-29.js')(),
            fixture('recipes', '30')(),
          ]);
          sinon.spy(BRest, '$sendRequest');
        });

        afterEach(function() {
          BRest.$sendRequest.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('expandAllByPage').that.is.a('Function');
        });

        it('should call getAllByPage', function() {
          var options = {test: 'test'};
          var restResource = new RestResource(xhr.xhttp);

          sinon.spy(restResource, 'getAllByPage');

          restResource.expandAllByPage(options);
          expect(restResource.getAllByPage).to.have.been.calledOnce();
          expect(restResource.getAllByPage).to.have.been.calledWith(options);
        });

        it('should call each resource of getAllByPage (with options if any) and return a promise containing pages ' +
          'of resources', function() {
            var options = {test: 'test'};
            var restResource = new RestResource(xhr.xhttp);

            var res = restResource.expandAllByPage(options);
            expect(res).to.be.an.instanceOf(Promise);

            var nbOfElements = 31;
            var nbOfGetAllCalls = 3;

            return res.then(function(res) {
              expect(BRest.$sendRequest.callCount).to.equal(nbOfGetAllCalls + nbOfElements);
              expect(res).to.be.an('object');
              expect(_.size(res)).to.equal(_.ceil(31 / 10)); // number of pages (10 is the default size in these examples)
              for (var i = 0; i < _.ceil(31 / 10); ++i) {
                expect(res[i]).to.be.an('array');
                for (var j = 0; j < res[i].length; ++j) {
                  expect(res[i][j]).to.be.an.instanceOf(RestResource);
                }
              }
            });
          })
        ;
      });
    });

    describe('on entity API', function() {
      before(function() {
        xhr.setDefaultsObject(fixture('recipe')('mousse-chocolat'));
      });

      // TODO type handling
      describe.skip('$schema', function() {
        it('should be initialized on demand to undefined if responseText does not contain a schema', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$schema').that.is.undefined();
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should be initialized on demand if responseText contains a schema (type + context)', function() {
          xhr.setBody('{"id":"mousse-chocolat","@context":"http://schema.org/","@type":"Recipe"}');

          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$schema').that.is.an('Object').that.deep.equal({
            context: 'http://schema.org/',
            type: 'Recipe'
          });
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should be initialized on demand if responseText contains a schema (just the context)', function() {
          xhr.setBody('{"id":"mousse-chocolat","@context":"http://schema.org/Recipe"}');

          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('$schema').that.is.an('Object').that.deep.equal({
            context: 'http://schema.org/Recipe',
          });
          expect(BRest.$initOnDemand).to.have.been.called();
        });

        it('should remove schema from content', function() {
          xhr.setBody('{"id":"mousse-chocolat","@context":"http://schema.org/","@type":"Recipe"}');

          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.not.have.property('@context');
          expect(restResource).to.not.have.property('@type');
          expect(BRest.$initOnDemand).to.have.been.called();
        });
      });

      describe('populate', function() {
        var ingredients;

        before(function() {
          BRest.$sendRequest = function(method, url, options) {
            var id;
            if (url.match(/\/([\w-]+)\/ingredients$/)) {
              id = RegExp.$1;
              return Promise.resolve(xhr(fixture('recipe', 'ingredients')(id)));
            }

            if (url.match(/\/([\w-]+)\/rate$/)) {
              id = RegExp.$1;
              return Promise.resolve(xhr(fixture('recipe', 'rate')(id)));
            }

            if (url.match(/\/rate\/5$/)) {
              return Promise.resolve(xhr(fixture('rate')()));
            }

            var parsedUrl = url.match(/\/ingredients\/([\w-]+)$/);
            id = RegExp.$1;
            return Promise.resolve(xhr(fixture('ingredient')(id)));
          };
        });

        beforeEach(
          function() {
          sinon.spy(RestResource.prototype, 'follow');
          sinon.spy(RestResource.prototype, 'expandAll');
        });

        afterEach(function() {
          RestResource.prototype.follow.restore();
          RestResource.prototype.expandAll.restore();
        });

        it('should be defined as a function', function() {
          var restResource = new RestResource(xhr.xhttp);
          expect(restResource).to.have.property('populate').that.is.a('Function');
        });

        it('should return a Promise resolved with the populated resource', function() {
            var restResource = new RestResource(xhr.xhttp);

            var res = restResource.populate();
            expect(res).to.be.an.instanceOf(Promise);

            return res.then(function(res) {
              expect(res).to.equal(restResource);
            });
          })
        ;

        it('should populate each link of the resource if no links given with the corresponding hashMap or ' +
          'resource (and transmitting options if any)', function() {
            var options = {test: 'test'};
            var restResource = new RestResource(xhr.xhttp);

            return restResource.populate(options).then(function(res) {
              expect(res.follow.callCount).to.equal(_.size(restResource.$links));
              expect(res.follow).to.have.been.calledWith('ingredients', options);
              var ingredientsPromise = res.follow.getCall(0).returnValue;
              return ingredientsPromise.then(function(ingredients) {
                expect(ingredients.expandAll).to.have.been.calledOnce();
                expect(ingredients.expandAll).to.have.been.calledWith(options);
                expect(res.ingredients).to.be.an('Object');
                expect(_.size(res.ingredients)).to.equal(3);
                expect(_.every(res.ingredients, function(ingredient) {
                  return ingredient instanceof RestResource;
                }));
                expect(res.follow).to.have.been.calledWith('rate', options);
                expect(res.rate).to.be.an.instanceOf(RestResource);
                expect(res.follow).to.have.been.calledWith('comment', options);
                expect(res.comment).to.be.an.instanceOf(RestResource);
              });
            });
          })
        ;

        it('should populate only the given existing links if an array of links is given', function() {
          var restResource = new RestResource(xhr.xhttp);

          return restResource.populate(['rate', 'comment', 'notExistingLink']).then(function(res) {
            expect(res.follow.callCount).to.equal(2);
            expect(res.follow).not.to.have.been.calledWith('notExistingLink');
            expect(res.notExistingLink).to.be.undefined();
            expect(res.follow).not.to.have.been.calledWith('ingredients');
            expect(res.ingredients).to.be.undefined();
            expect(res.follow).to.have.been.calledWith('rate');
            expect(res.rate).to.be.an.instanceOf(RestResource);
            expect(res.follow).to.have.been.calledWith('comment');
            expect(res.comment).to.be.an.instanceOf(RestResource);
          });
        });

        it('should populate only the given existing link if a single link is given', function() {
          var restResource = new RestResource(xhr.xhttp);

          return restResource.populate('comment').then(function(res) {
            expect(res.follow.callCount).to.equal(1);
            expect(res.follow).not.to.have.been.calledWith('ingredients');
            expect(res.ingredients).to.be.undefined();
            expect(res.follow).not.to.have.been.calledWith('rate');
            expect(res.rate).to.be.undefined();
            expect(res.follow).to.have.been.calledWith('comment');
            expect(res.comment).to.be.an.instanceOf(RestResource);
          });
        });

        it('should populate nothing if the given link does not exist', function() {
          var restResource = new RestResource(xhr.xhttp);

          return restResource.populate('notExistingLink').then(function(res) {
            expect(res.follow.callCount).to.equal(0);
            expect(res.follow).not.to.have.been.calledWith('notExistingLink');
            expect(res.notExistingLink).to.be.undefined();
          });
        });
      });
    });
  });
});
