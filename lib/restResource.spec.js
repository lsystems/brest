'use strict';

var chai = require('chai');
var sinon = require('sinon');
var expect = chai.expect;
require('../utils/test');

var RestResourceModule = require('./restResource');

describe('module restResource', function() {
  var BRest = {};

  it('should be a function', function() {
    expect(RestResourceModule).to.be.a('Function');
  });

  it('should return nothing', function() {
    expect(RestResourceModule(BRest)).to.be.undefined();
  });

  it('should define RestResource', function() {
    RestResourceModule(BRest);
    expect(BRest.RestResource).to.be.a('Function');
  });

  it('should define $builder as an abstract function', function() {
    RestResourceModule(BRest);
    var restResource = new BRest.RestResource();
    expect(restResource).to.have.a.property('$builder').that.is.a('Function');
    var thrown = function() {
      restResource.$builder();
    };
    expect(thrown).to.throw('abstract');
  });
});
