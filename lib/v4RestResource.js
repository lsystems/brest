'use strict';

var _ = require('lodash');
var util = require('util');

module.exports = function(BRest) {

  BRest.v4 = {};

  /**
   * @alias module:BRest.v4.RestResource
   * @constructor
   * @extends module:BRest.RestResource
   * @private
   * @classdesc `RestResource` for v4 API (HATEOAS, header use)
   * @param {BRest.Request} xhttp HTTP call result of the existing resource
   * @param {Object} [options] Options used to customize the RestResource generation
   *   @param {Function} options.parser This is used to parse your API body data if provided. `JSON.parse` is used by default.
   * @throws if:
   *   - the xhttp body response is not JSON but no parse has been given in the options
   *   - the xhttp body response can't be parsed as an object
   *   - if multiple types other than JSON or LD are found in Content-Type header
   */
  var RestResource = function RestResource(xhttp, options) {
    if (xhttp.status >= 400) {
      console.debug(xhttp);
      this.status = xhttp.status;
      return this;
    }

    var schema;
    var links = {};

    BRest.$initOnDemand(this, {
      $apiVersion: function() {
        return 'v4';
      },
      $self: function() {
        return xhttp.getResponseHeader('Content-Location') || xhttp.responseURL;
      },
      $version: function() {
        return xhttp.getResponseHeader('ETag') || 0;
      },
      $type: function() {
        var fullContentType = xhttp.getResponseHeader('Content-Type').toLowerCase();
        var secondPart = fullContentType.split('/')[1];
        var types = _.difference(secondPart.split('+'), ['json', 'ld']);

        if (types.length > 1) {
          throw new Error('Multiple types found');
        }
        return _.first(types);
      },
      $schema: function() {
        return schema;
      },
      $links: function() {
        return _.pickBy(links, function(value, link) {
          return _.isNaN(parseInt(link));
        });
      },
      $isCollection: function() {
        return xhttp.getResponseHeader('Content-Type').toLowerCase().search('collection') > 0;
      },
      $collection: function() {
        return _.filter(links, function(value, link) {
          return !_.isNaN(parseInt(link));
        });
      },
      $hashMap: function() {
        return _.reduce(links, function(links, value, link) {
          var numValue = parseInt(link);
          if (!_.isNaN(numValue)) {
            links[numValue] = value;
          }
          return links;
        }, {});
      },
      $range: function() {
        var rangeHeader = xhttp.getResponseHeader('Content-Range');
        if (!rangeHeader) {
          return {};
        }

        var matchs = rangeHeader.match(/(\w+) (?:(\d+)-)?(\d+)\/(\d+)/);
        var range = {
          type: matchs[1],
          end: parseInt(matchs[3]),
          total: parseInt(matchs[4]),
        };

        range.start = matchs[2] ? parseInt(matchs[2]) : range.end;
        range.size = range.end - range.start + 1;

        return range;
      },
      $navigation: function() {
        var rangeHeader = xhttp.getResponseHeader('Content-Range');
        if (!rangeHeader) {
          return {};
        }

        var matchs = rangeHeader.match(/(\w+) (?:(\d+)-)?(\d+)\/(\d+)/);
        if (!matchs) {
          throw new Error('Invalid range header ' + rangeHeader);
        }

        var type = matchs[1];
        var end = parseInt(matchs[3]);
        var total = parseInt(matchs[4]);
        var start = matchs[2] ? parseInt(matchs[2]) : end;
        var size = end - start + 1;

        var first = 0;
        var last = total - 1;
        var firstPage = 0;
        var lastPage = Math.floor(last / size);
        var currentPage = Math.floor(start / size);
        var currentPageStart = Math.floor(start / size) * size;

        var rangeTemplate = function(start, end) {
          return start === Math.min(end, last) ?
            util.format(type + ' %s', start) :
            util.format(type + ' %s-%s', start, Math.min(end, last));
        };

        var navigation = {};

        navigation.step = function(step) {
          return rangeTemplate(0, step - 1);
        };

        if (start > first) {
          navigation.first = rangeTemplate(0, size - 1);
          if (currentPage > firstPage) {
            navigation.prev = rangeTemplate(currentPageStart - size, currentPageStart - 1);
          }
        }

        if (end < last) {
          navigation.last = rangeTemplate(lastPage * size, last);
          if (currentPage < lastPage) {
            navigation.next = rangeTemplate(currentPageStart + size, currentPageStart + 2 * size - 1);
          }
        }

        navigation.current = rangeTemplate(currentPageStart, currentPageStart + size - 1);

        return navigation;
      },
      $generalOptions: function() {
        return options || {};
      },
      /**
       * @alias module:BRest.v4.RestResource~$builder
       * @summary Instanciate a new v4 Resource from a `BRest.Request`
       * @private
       * @param {BRest.Request} xhttp HTTP call response
       * @return {RestResource} Built resource
       */
      $builder: function() {
        return function(res) {
          return new RestResource(res, options);
        };
      },
    });

    var linkHeader = xhttp.getResponseHeader('Link');
    if (linkHeader) {
      links = _.reduce(linkHeader.split(','), function(links, link) {
        var matchs = link.trim().match(/^<(.*)>; rel="(.*)"$/);
        if (matchs) {
          links[matchs[2]] = matchs[1];
        }
        return links;
      }, {});
    }

    if (!xhttp.responseBody) {
      return;
    }

    var json = /\/([^\/;]*)json/.test(xhttp.getResponseHeader('Content-Type'));
    if (!json && (!options || !options.parser)) {
      throw new Error('Non-JSON response need a parser');
    }

    var content;
    try {
      content = (options && options.parser || JSON.parse)(xhttp.responseBody);
    }
    catch (e) {
      throw new Error('Body parsing failed ' + content);
    }

    if (!_.isObject(content)) {
      throw new Error('Body parsing failed ' + content);
    }

    // TODO type handling
    /*if (content && content['@context']) {
      schema = {
        context: content['@context'],
      };
      delete content['@context'];

      if (content['@type']) {
        schema.type = content['@type'];
        delete content['@type'];
      }
    }*/

    _.extend(this, content);
  };

  RestResource.super_ = BRest.RestResource;
  RestResource.prototype = Object.create(BRest.RestResource.prototype, {
    constructor: {
      value: RestResource,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });

  /**
   * @inheritdoc
   */
  RestResource.prototype.$resolveResource = _.partialRight(RestResource.super_.prototype.$resolveResource, 'HEAD');

  /**
   * @inheritdoc
   */
  RestResource.prototype.$resolveCollection = function(range, options) {
    return this.$resolveResource(this.$self, _.extend({}, options, {
      headers: {
        range: range,
      },
    }));
  };

  BRest.v4.RestResource = RestResource;
};
