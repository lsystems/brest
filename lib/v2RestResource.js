'use strict';

var Url = require('url');
var _ = require('lodash');
var Promise = require('bluebird');

module.exports = function(BRest) {

  BRest.v2 = {};

  /**
   * @alias module:BRest.v2.RestResource
   * @constructor
   * @extends module:BRest.RestResource
   * @private
   * @classdesc `RestResource` for v2 API (non-HATEOAS, non-header use)
   * @param {BRest.Request|String} xhttp HTTP call result of the existing resource (not in case of summary) or the rootUrl if your API doesn't have a summary (avoid a useless HTTP call). if you use an API that have no summary/root (like REST v2 API), you can provide this `rootUrl` option to avoid a useless API determination call to your unexisting API root. It overrides `url` parameter if both are set.
   * @param {Object} [options] Options used to customize the RestResource generation
   *   @param {Boolean|String[]} [options.hasSlash=false] Define if all or some API URIs contain a trailing slash (using their ids)
   *   @param {Function} options.parser This is used to parse your API body data if provided. `JSON.parse` is used by default.
   * @throws if:
   *   - the xhttp body response is not JSON but no parse has been given in the options
   *   - the xhttp body response can't be parsed as an object
   */
  var RestResource = function RestResource(xhttp, options) {
    if (!(xhttp instanceof BRest.Request) && !_.isString(xhttp)) {
      throw new Error('An URL or a BRest.Request is required');
    }

    var isCollection = false;

    BRest.$initOnDemand(this, {
      $apiVersion: function() {
        return 'v2';
      },
      $generalOptions: function() {
        return options || {};
      },
      /**
       * @alias RestResource~$self
       * @property {String} $self The current rootUrl defined by the so-called "summary" for a v2 API or the current resource URL
       * @private
       */
      $self: function() {
        return xhttp;
      },
      $version: function() {
        return 0;
      },
      /**
       * @alias RestResource~$links
       * @property {Object} [$links={}] Return an empty object (no links in non-HATEOAS API)
       */
      $links: function() {
        return {};
      },
      $isCollection: function() {
        return isCollection;
      },
      /**
       * @alias module:BRest.v2.RestResource~$builder
       * @summary Instanciate a new v2 Resource from a `BRest.Request`
       * @private
       * @param {BRest.Request} xhttp HTTP call response
       * @return {RestResource} Built resource
       */
      $builder: function() {
        return function(xhttp) {
          return new RestResource(xhttp, options);
        };
      },
    });

    if (!(xhttp instanceof BRest.Request)) {
      return;
    }

    if (xhttp.status >= 400) {
      console.debug(xhttp);
      this.status = xhttp.status;
      return this;
    }

    var hashMap = {};
    var type;
    var count;
    var start;
    var total;
    var schema;

    BRest.$initOnDemand(this, {
      $self: function() {
        return xhttp.responseURL;
      },
      $version: function() {
        return xhttp.getResponseHeader('ETag') || 0;
      },
      $schema: function() {
        return schema;
      },
      $collection: function() {
        return _.values(hashMap);
      },
      $hashMap: function() {
        return hashMap;
      },
      $range: function() {
        var range = {};

        if (type) {
          range.type = type;
        }

        if (!_.isNaN(start)) {
          range.start = start;
        }

        if (!_.isNaN(total)) {
          range.total = total;
        }

        if (!_.isNaN(start) && !_.isNaN(count)) {
          range.end = start + count - 1;
        }

        if (!_.isNaN(count)) {
          range.count = count;
        }

        return range;
      },
      $navigation: function() {
        if (!type || _.isNaN(start) || _.isNaN(total) || _.isNaN(count)) {
          return {};
        }

        var navigation = {
          current: xhttp.responseURL,
          step: function(step) {
            var root = Url.parse(xhttp.responseURL, true);
            root.query.type = type;
            root.query.size = step;
            root.query.start = 0;
            return Url.format(root);
          },
        };

        var root = Url.parse(xhttp.responseURL, true);
        delete root.search;

        root.query.type = type;
        root.query.size = count;

        if (start > 0) {
          navigation.first = Url.format(_.set(_.cloneDeep(root), 'query.start', 0));
          navigation.prev = Url.format(_.set(_.cloneDeep(root), 'query.start', Math.max(start - root.query.size, 0)));
        }

        if (start + root.query.size < total) {
          var page = Math.floor((total - 1) / root.query.size);
          navigation.last = Url.format(_.set(_.cloneDeep(root), 'query.start', page * root.query.size));
          navigation.next = Url.format(_.set(_.cloneDeep(root), 'query.start', start + root.query.size));
        }

        return navigation;
      },
    });

    if (!xhttp.responseBody) {
      return;
    }

    var json = /\/([^\/;]*)json/.test(xhttp.getResponseHeader('Content-Type'));
    if (!json && (!options || !options.parser)) {
      throw new Error('Non-JSON response need a parser');
    }

    var content;
    try {
      content = (options && options.parser || JSON.parse)(xhttp.responseBody);
    }
    catch (e) {
      throw new Error('Body parsing failed ' + content);
    }

    if (!_.isObject(content)) {
      throw new Error('Body parsing failed ' + content);
    }

    // TODO v1 (no links) handling
    /*if (_.isArray(content)) {
      isCollection = true;
      hashMap = _.reduce(content, function(acc, value, key) {
        acc[key] = value;
        return acc;
      }, {});
      return;
    }

    // TODO type handling
    if (content && content['@context']) {
      schema = {
        type: content['@context'],
      };
      delete content['@context'];

      if (content['@type']) {
        schema.context = content['@type'];
        delete content['@type'];
      }
    }*/

    if (content && content.elements) {
      isCollection = true;

      count = parseInt(content.count);
      start = parseInt(content.start);
      total = parseInt(content.total);
      type = content.type;

      hashMap = _.reduce(content.elements, function(acc, element, index) {
        acc[index + start] = element;
        return acc;
      }, {}, this);

      return;
    }

    _.extend(this, content);
  };

  RestResource.super_ = BRest.RestResource;
  RestResource.prototype = Object.create(BRest.RestResource.prototype, {
    constructor: {
      value: RestResource,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });

  /**
   * @summary Generate the URL from the resource id and the root URL, whether it requires a / or not
   * @private
   * @param {String} linkName Link name is the resource ID which the URL is generated for
   * @return {String} The generated URL for the given linkName
   */
  RestResource.prototype.$getLink = function(linkName) {
    if (BRest.$urlValidator(linkName)) {
      return linkName;
    }

    var removeStartAndEndSlashes = function(string) {
      var noStart = _.startsWith(string, '/') ? string.slice(1) : string;
      return _.endsWith(noStart, '/') ? noStart.slice(0, -1) : noStart;
    };

    var url = Url.parse(this.$self);
    delete url.search;
    delete url.query;
    url.pathname = removeStartAndEndSlashes(url.pathname) + '/' + removeStartAndEndSlashes(linkName);
    return Url.format(url) +
      (this.$generalOptions.hasSlash && (_.isBoolean(this.$generalOptions.hasSlash) ||
        _.contains(this.$generalOptions.hasSlash, linkName)) ? '/' : '');
  };

  /**
   * @inheritdoc
   */
  RestResource.prototype.$resolveResource = function(id, options) {
    return RestResource.super_.prototype.$resolveResource.call(this, this.$getLink(id), options, 'GET');
  };

  /**
   * @inheritdoc
   */
  RestResource.prototype.follow = function(linkName, options) {
    if (!this[linkName]) {
      return RestResource.prototype.$resolveResource.apply(this, arguments);
    }

    var xhttp = new BRest.Request();
    if (!_.isArray(this[linkName])) {
      xhttp.responseBody = this[linkName];
    } else {
      xhttp.responseBody = {
        start: 0,
        count: this[linkName].length,
        total: this[linkName].length,
        type: linkName,
        elements: this[linkName],
      };
    }
    xhttp.status = 200;
    xhttp.responseURL = this.$getLink(linkName);
    var generatedResource = new RestResource(xhttp, _.merge({}, options, {
      parser: _.identity
    }));

    // parser must not be propagated till it's only due to resource generation
    delete generatedResource.$generalOptions.parser;
    if (options && options.parser) {
      generatedResource.$generalOptions.parser = options.parser;
    }

    return Promise.resolve(generatedResource);
  };

  /**
   * @summary Populate a given link of the current resource
   * @param {String|Array} linkName Name or names of links to populate
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The populated RestResource
   */
  RestResource.prototype.populate = function(linkName, options) {
    var linkNames = _.isArray(linkName) ? linkName : [linkName];

    return Promise.each(linkNames, function(linkName) {
      return this.follow(linkName, options)
        .then(function(linkValue) {
          if (!linkValue.$isCollection) {
            return linkValue;
          }
          return linkValue.expandAll(options);
        })
        .then(function(expandedLinkValue) {
          this[linkName] = expandedLinkValue;
        }.bind(this))
      ;
    }.bind(this)).return(this);
  };

  BRest.v2.RestResource = RestResource;
};
