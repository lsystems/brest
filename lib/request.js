'use strict';

module.exports = function(BRest) {
  var Request = function() {
    this.status = 0;
    this.responseURL = null;
    this.responseHeaders = {};
    this.responseBody = null;
  };

  Request.prototype.getResponseHeader = function(name) {
    if (!this._xhttp) {
      return this.responseHeaders[name.toLowerCase()];
    }

    return this.responseHeaders[name.toLowerCase()] || this._xhttp.getResponseHeader.apply(this._xhttp, arguments);
  };

  Request.prototype.addResponseHeader = function(name, value) {
    this.responseHeaders[name.toLowerCase()] = value;
    return this;
  };

  Request.prototype.getAllResponseHeaders = function() {
    return this._xhttp ? this._xhttp.getAllResponseHeaders() : '';
  };

  Request.fromXMLHttpRequest = function(xhttp) {
    var request = new Request();

    request._xhttp = xhttp;

    request.status = xhttp.status;
    request.responseURL = xhttp.responseURL;
    request.responseBody = xhttp.responseText || xhttp.response;
    return request;
  };

  BRest.Request = Request;
};
