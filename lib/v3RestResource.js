'use strict';

var Url = require('url');
var _ = require('lodash');

module.exports = function(BRest) {

  BRest.v3 = {};

  /**
   * @alias module:BRest.v3.RestResource
   * @constructor
   * @extends module:BRest.RestResource
   * @private
   * @classdesc `RestResource` for v3 API (HATEOAS, non-header use)
   * @param {BRest.Request} xhttp HTTP call result of the existing resource
   * @param {Object} [options] Options used to customize the RestResource generation
   *   @param {String} options.linkPrefix If the links has a given prefix, use this instead of checking if each attribute is an URL or not
   *   @param {Function} options.parser This is used to parse your API body data if provided. `JSON.parse` is used by default.
   * @throws if:
   *   - the xhttp body response is not JSON but no parse has been given in the options
   *   - the xhttp body response can't be parsed as an object
   */
  var RestResource = function RestResource(xhttp, options) {
    if (xhttp.status >= 400) {
      console.debug(xhttp);
      this.status = xhttp.status;
      return this;
    }

    var links = {};
    var hashMap = {};
    var count = 0;
    var schema;
    var isCollection = false;

    BRest.$initOnDemand(this, {
      $apiVersion: function() {
        return 'v3';
      },
      $self: function() {
        return links.self || xhttp.responseURL;
      },
      $version: function() {
        return xhttp.getResponseHeader('ETag') || 0;
      },
      $schema: function() {
        return schema;
      },
      $links: function() {
        return _.omit(links, ['self', 'first', 'last', 'prev', 'next']);
      },
      $isCollection: function() {
        return isCollection;
      },
      $collection: function() {
        return _.values(hashMap);
      },
      $hashMap: function() {
        return hashMap;
      },
      $range: function() {
        var weight;
        var urls = [{
          value: links.self || xhttp.responseURL,
          weight: 0,
        }, {
          value: links.next,
          weight: 1,
        }, {
          value: links.prev,
          weight: -1,
        }];

        var validParsedUrl;
        for (var i = 0; i < urls.length; i++) {
          if (!urls[i].value) {
            continue;
          }

          var parsedUrl = Url.parse(urls[i].value, true);
          if (!_.size(parsedUrl.query) || !parsedUrl.query.type) {
            continue;
          }
          validParsedUrl = parsedUrl;

          weight = urls[i].weight;
          break;
        }

        if (!validParsedUrl) {
          return {};
        }

        var start = parseInt(validParsedUrl.query.start) || 0;
        var size = parseInt(validParsedUrl.query.size) || _.size(hashMap);

        var range = {
          type: validParsedUrl.query.type,
          start: start - size * weight,
          end: start  + size - 1 - size * weight,
          total: parseInt(count),
          size: size,
        };

        return range;
      },
      $navigation: function() {
        if (!isCollection) {
          return {};
        }

        var originalCount = Url.parse(links.self, true).query.size;
        originalCount = originalCount ? parseInt(originalCount) : _.size(hashMap);

        var type;

        return _.reduce(['first', 'last', 'prev', 'next', 'current'], function (acc, linkName) {
          var link = links[linkName === 'current' ? 'self' : linkName];
          if (!link) {
            return acc;
          }
          var processedRange = Url.parse(link, true);
          if (!type && processedRange.query.type) {
            type = processedRange.query.type;
          }

          delete processedRange.search;
          acc[linkName] = Url.format(processedRange);
          return acc;
        }, {
          step: function(step) {
            var processedRange = Url.parse(links.self, true);
            processedRange.query.type = type;
            processedRange.query.start = 0;
            processedRange.query.size = step;
            return Url.format(processedRange);
          },
        });
      },
      $generalOptions: function() {
        return options || {};
      },
      /**
       * @alias module:BRest.v3.RestResource~$builder
       * @summary Instanciate a new v3 Resource from a `BRest.Request`
       * @private
       * @param {BRest.Request} xhttp HTTP call response
       * @return {RestResource} Built resource
       */
      $builder: function() {
        return function(res) {
          return new RestResource(res, options);
        };
      },
    });

    if (!xhttp.responseBody) {
      return;
    }

    var json = /\/([^\/;]*)json/.test(xhttp.getResponseHeader('Content-Type'));
    if (!json && (!options || !options.parser)) {
      throw new Error('Non-JSON response need a parser');
    }

    var content;
    try {
      content = (options && options.parser || JSON.parse)(xhttp.responseBody);
    }
    catch (e) {
      throw new Error('Body parsing failed ' + content);
    }

    if (!_.isObject(content)) {
      throw new Error('Body parsing failed ' + content);
    }

    // TODO type handling
    /*if (content && content['@context']) {
      schema = {
        context: content['@context'],
      };
      delete content['@context'];

      if (content['@type']) {
        schema.type = content['@type'];
        delete content['@type'];
      }
    }*/

    _.forIn(content, function(value, key) {
      if (!BRest.$urlValidator(value) ||
        (options && options.linkPrefix && !key.match(new RegExp('^' + options.linkPrefix)))) {
        return;
      }
      delete content[key];

      if (options && options.linkPrefix && key.match(new RegExp('^' + options.linkPrefix))) {
        key = key.substring(options.linkPrefix.length);
      }

      links[key] = value;
    }, this);

    isCollection = _.isArray(content.elements);
    if (isCollection) {
      var start = parseInt((Url.parse(links.self || content.self || xhttp.responseURL, true).query || {}).start || 0);
      hashMap = _.reduce(content.elements, function(acc, element, index) {
        acc[index + start] = element;
        return acc;
      }, {}, this);
      delete content.elements;

      count = content.count;
      delete content.count;
    }

    _.extend(this, content);
  };

  RestResource.super_ = BRest.RestResource;
  RestResource.prototype = Object.create(BRest.RestResource.prototype, {
    constructor: {
      value: RestResource,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });

  /**
   * @inheritdoc
   */
  RestResource.prototype.$resolveResource = _.partialRight(RestResource.super_.prototype.$resolveResource, 'GET');

  /**
   * @inheritdoc
   */
  RestResource.prototype.populate = function(linkName, options) {
    return RestResource.super_.prototype.populate.apply(this, arguments).then(function(res) {
      if (!res.$generalOptions.linkPrefix) {
        return res;
      }

      _.forEach(res.$links, function(link, linkName) {
        if (!res[linkName]) {
          return;
        }
        res[res.$generalOptions.linkPrefix + linkName] = res[linkName];
        delete res[linkName];
      });

      return res;
    });
  };

  BRest.v3.RestResource = RestResource;
};
