'use strict';

var Promise = require('bluebird');
var _ = require('lodash');

module.exports = function(BRest) {

  /**
   * @alias module:BRest.RestResource
   * @constructor
   * @classdesc Default REST resource behaviors common to each API version
   */
  var RestResource = function RestResource() {
    /**
     * @property {String} $apiVersion Current API version
     * @private
     */
    this.$apiVersion = undefined;
    /**
     * @property {Object} $generalOptions Options that will be transmitted to children `RestResource`
     * @private
     */
    this.$generalOptions = undefined;
    /**
     * @property {String} $self The current resource URL
     * @private
     */
    this.$self = undefined;
    /**
     * @property {String} $version If exists, it's the current version of the resource. It is used to check if the resource has been updated
     * @private
     */
    this.$version = undefined;
    /**
     * @property {Object} $schema If exists, define the resource type (see JSON-LD for more information)
     *   @property {String} [$schema.context] Define the resource context (see schema.org for more information)
     *   @property {String} $schema.type Define the resource type
     * @private
     */
    this.$schema = undefined;
    /**
     * @property {Object} $links Contains the different registered links (URL) to other REST resources (HATEOAS)
     * @private
     */
    this.$links = undefined;
    /**
     * @property {Boolean} $isCollection Returns if the resource is a collection or not
     * @private
     */
    this.$isCollection = undefined;
    /**
     * @property {Array} $collection Returns the collection page identifiers
     * @private
     */
    this.$collection = undefined;
    /**
     * @property {Object} $hashMap Returns the collection page hash map
     * @private
     */
    this.$hashMap = undefined;
    /**
     * @property {Object} $range The range object for navigation like for headers
     *   @property {String} $range.type The type of the filter on this collection
     *   @property {Number} $range.start The position of the first element of this page in the collection (starts at 0)
     *   @property {Number} $range.end The position of the last element of this page in the collection
     *   @property {Number} $range.size The number of elements in the current page of the collection
     *   @property {Number} $range.total The number of elements in the whole collection
     *   @property {Number} $range.step The requested step of this collection (different from size on the last chunk)
     * @private
     */
    this.$range = undefined;
    /**
     * @summary Returns the navigation data
     * @returns {Object} The navigation object that is used to navigate over a collection
     *   @returns {String} [$navigation.current] The navigation data to the current page depending on size
     *   @returns {String} [$navigation.first] The navigation data to navigate to the first page of the collection
     *   @returns {String} [$navigation.last] The navigation data to navigate to the last page of the collection
     *   @returns {String} [$navigation.next] The navigation data to navigate to the next page of the collection
     *   @returns {String} [$navigation.prev] The navigation data to navigate to the previous page of the collection
     *   @returns {Function} [$navigation.step] Determine the first page for a given step
     *     @returns {String} The navigation data for the first page with a given step
     * @private
     */
    this.$navigation = undefined;
  };

  /**
   * @summary Generic method to instanciate a new Resource from a `BRest.Request`
   * @abstract
   * @private
   * @throws {Error} if not overriden (abstract)
   */
  RestResource.prototype.$builder = function() {
    throw new Error('$builder is abstract. It must be overriden in higher-level RestResource');
  };

  /**
   * @summary Define how to resolve a resource (HTTP call for instance)
   * @private
   * @param {String} id ID (or URL in HATEOAS) of the resource to load
   * @param {Object} [options] for the HTTP call
   * @param {String} [methodName] HTTP method name which is used by the HTTP call
   * @returns {Promise(RestResource)} Resolved resource
   */
  RestResource.prototype.$resolveResource = function(id, options, methodName) {
    return BRest.$sendRequest(methodName, id, _.extend({}, this.$generalOptions, options))
      .then(this.$builder);
  };

  /**
   * @summary Define how to resolve a collection, defaulted to a resource resolution
   * @private
   * @param {String} id ID (or URL in HATEOAS) of the resource to load
   * @param {Object} [options] Options of the HTTP call
   * @returns {Promise(RestResource)} Resolved collection
   */
  RestResource.prototype.$resolveCollection = function(url, options) {
    return this.$resolveResource(url, options);
  };

  /**
   * @summary Follow a given link and return the related resource
   * @param {String} linkName Link name or resource name (non-HATEOAS) of the resource to load
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} Followed resource
   * @throws {Error} if the link doesn't exist for the current resource
   *
   * @example
   * <caption>Simple example to access to github repositories</caption>
   * BRest.RestApi('https://api.github.com').then(function(restApiResource) {
   *   return restApiResource.follow('user/repos');
   * }).then(function(repoCollectionResource) {
   *   // Here you can use your repository collection
   * });
   */
  RestResource.prototype.follow = function(linkName, options) {
    if (!_.isString(this.$links[linkName])) {
      throw new Error('Not a link: ' + linkName);
    }

    return this.$resolveResource(this.$links[linkName], options);
  };

  /**
   * @summary Get a given entity from a collection by its index
   * @param {String|Number} index Index of the entity
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} Resource corresponding to the given index of the collection
   * @throws {Error} :
   * - if the current RestResource is not a collection
   * - if the current RestResource has no entity for this index
   *
   * @example
   * <caption>Simple example to get a specific github repository</caption>
   * BRest.RestApi('https://api.github.com').then(function(restApiResource) {
   *   return restApiResource.follow('user/repos');
   * }).then(function(repoCollectionResource) {
   *   return repoCollectionResource.get(1);
   * }).then(function(secondRepository) {
   *  // Here you have access to the second repository entity
   * });
   */
  RestResource.prototype.get = function(index, options) {
    if (!this.$isCollection) {
      throw new Error('Not a collection');
    }

    if (!this.$hashMap[index]) {
      throw new Error('No entity found');
    }

    return this.$resolveResource(this.$hashMap[index], options, 'GET');
  };

  /**
   * @summary Expand all resources of this collection resource page
   * @param {Object} [options] Options for the HTTP calls
   * @returns {Promise(RestResource[])} Expanded resources
   * @throws {Error} if the current resource is not a collection
   *
   * @example
   * <caption>Simple example to get all github repositories of the first page of the collection</caption>
   * BRest.RestApi('https://api.github.com').then(function(restApiResource) {
   *   return restApiResource.follow('user/repos');
   * }).then(function(repoCollectionResource) {
   *   return repoCollectionResource.expand();
   * }).then(function(expandedRepos) {
   *   // Here you have access to all your repositories (limited to the first page of the collection, if paginated)
   * });
   */
  RestResource.prototype.expand = function(options) {
    if (!this.$isCollection) {
      throw new Error('Not a collection');
    }

    return Promise.map(this.$collection, function(url) {
      return this.$resolveResource(url, options);
    }.bind(this));
  };

  /**
   * @summary Get the current pagination step on this collection
   * @returns {Number} The pagination step
   */
  RestResource.prototype.getStep = function() {
    return this.$range.step;
  };

  /**
   * @summary Change the pagination step on this collection
   * @param {Number} step Number of elements fetch by page on navigation
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The requested collection page with the new step
   * @throws {Error} if:
   *   - the size is not a valid number
   *   - the current resource is not a collection
   */
  RestResource.prototype.setStep = function(step, options) {
    if (step && !_.isNumber(step)) {
      throw new Error('Not a valid step ' + step);
    }

    if (!this.$isCollection) {
      throw new Error('Not a collection');
    }

    return this.$resolveCollection(this.$navigation.step(step), options);
  };

  /**
   * @summary Generic method to navigate over the collection. Used by `next`, `prev`, `last`, `first`. The chosen step can be found in `this.$range.$size` and set in `this.setStep`.
   * @private
   * @param {String} way Way to follow on the collection (next, prev, first, last), corresponding to $navigation link names
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The requested collection page or the current resource if the requested page doesn't exist
   * @throws {Error} if:
   *   - the current resource is not a collection
   */
  RestResource.prototype.$navigate = function(way, options) {
    if (!this.$isCollection) {
      throw new Error('Not a collection');
    }

    if (!this.$navigation[way]) {
      return Promise.resolve(this);
    }

    return this.$resolveCollection(this.$navigation[way], options);
  };

  /**
   * @summary Navigate over the collection to the next collection page. To get or set pagination step, use `getStep` and `setStep`.
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The next collection page or the current resource if it is the last page
   *
   * @example
   * <caption>Simple example to access to github repository next page using a pagination of 5</caption>
   * BRest.RestApi('https://api.github.com').then(function(restApiResource) {
   *   return restApiResource.follow('user/repos');
   * }).then(function(repoCollectionResource) {
   *   return repoCollectionResource.next();
   * }).then(function(nextRepoPageResource) {
   *   // Here you can use your repository collection second page
   * });
   */
  RestResource.prototype.next = function(options) {
    return this.$navigate('next', options);
  };

  /**
   * @summary Navigate over the collection to the previous collection page. To get or set pagination step, use `getStep` and `setStep`.
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The previous collection page or the current resource if it is the first page
   *
   * @example
   * <caption>Simple example to access to github repository next page using a pagination of 5</caption>
   * BRest.RestApi('https://api.github.com').then(function(restApiResource) {
   *   return restApiResource.follow('user/repos');
   * }).then(function(repoCollectionResource) {
   *   return repoCollectionResource.next();
   * }).then(function(repoCollectionResource) {
   *   return repoCollectionResource.prev();
   * }).then(function(prevRepoPageResource) {
   *   // Here you can use your repository collection previous page (here, it is similar to the first page)
   * });
   */
  RestResource.prototype.prev = function(options) {
    return this.$navigate('prev', options);
  };

  /**
   * @function
   * @see module:BRest.RestResource#prev
   */
  RestResource.prototype.previous = RestResource.prototype.prev;

  /**
   * @summary Navigate over the collection to the first collection page. To get or set pagination step, use `getStep` and `setStep`.
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The first collection page or the current resource if already on the first page
   *
   * @example
   * <caption>Simple example to access to github repository first page paginated by 5</caption>
   * BRest.RestApi('https://api.github.com').then(function(restApiResource) {
   *   return restApiResource.follow('user/repos');
   * }).then(function(repoCollectionResource) {
   *   return repoCollectionResource.first();
   * }).then(function(firstRepoPageResource) {
   *   // Here you can use your repository collection first page
   * });
   */
  RestResource.prototype.first = function(options) {
    return this.$navigate('first', options);
  };

  /**
   * @summary Navigate over the collection to the last collection page. To get or set pagination step, use `getStep` and `setStep`.
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The last collection page or the current resource if already on the last page
   *
   * @example
   * <caption>Simple example to access to github repository last page using a pagination of 5</caption>
   * BRest.RestApi('https://api.github.com').then(function(restApiResource) {
   *   return restApiResource.follow('user/repos');
   * }).then(function(repoCollectionResource) {
   *   return repoCollectionResource.last();
   * }).then(function(lastRepoPageResource) {
   *   // Here you can use your repository collection last page
   * });
   */
  RestResource.prototype.last = function(options) {
    return this.$navigate('last', options);
  };

  /**
   * @summary Transform a RestResource in plain JS object. It is useful for debbuging and shows all hidden internal properties.
   * @returns {Object} Plain JS object containing all hidden properties of the RestResource object. Ready to be logged.
   */
  RestResource.prototype.toObject = function() {
    var summary = {
      $apiVersion: this.$apiVersion,
      $self: this.$self,
      $version: this.$version,
      $type: this.$type,
      $schema: this.$schema,
      $links: this.$links,
      $isCollection: this.$isCollection,
      $generalOptions: this.$generalOptions,
      body: _.assignWith({}, this, function customizer(objValue, srcValue) {
        return !_.isFunction(srcValue) ? srcValue : undefined;
      }),
    };

    if (this.$isCollection) {
      summary.$collection = this.$collection;
      summary.$hashMap = this.$hashMap;
      summary.$navigation = this.$navigation;
      summary.$range = this.$range;
    }

    return summary;
  };

  /**
   * @summary Transform a RestResource in a JSON, indented or not. It is useful for debbuging and shows all hidden internal properties.
   * @param {Boolean} [noIndent=true] Should the generated JSON be indented
   * @returns {String} JSON containing all hidden properties of the RestResource object. Ready to be logged.
   */
  RestResource.prototype.toString = function(noIndent) {
    return JSON.stringify(this.toObject(), null, _.isBoolean(noIndent) && noIndent ? 0 : 2);
  };

  /**
   * @summary Get all the collection elements at a time - not expanded
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(Object)} The requested enriched collection's elements in an hashmap, keeping the original positions
   */
  RestResource.prototype.getAll = function(options) {
    var allRecipes = {};

    var next = function(restResource) {
      _.merge(allRecipes, restResource.$hashMap);

      if (!restResource.$navigation.next) {
        return;
      }

      return restResource.next(options).then(next);
    };

    return this.first(options).then(next).return(allRecipes);
  };

  /**
   * @summary Get all collection at a time, but still separated by page - not expanded
   * @param {Number} [size] Size of the requested pages
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource[])} An array of collection's pages
   */
  RestResource.prototype.getAllByPage = function(size, options) {
    var allRecipesChunks = [];

    if (_.isObject(size)) {
      options = size;
      size = undefined;
    }

    var next = function(restResource) {
      allRecipesChunks.push(restResource);

      if (!restResource.$navigation.next) {
        return;
      }

      return restResource.next(options).then(next);
    };

    var start = size ? this.setStep(size, options) : this.first(options);

    return start.then(next).return(allRecipesChunks);
  };

  /**
   * @summary Expand the given links of the collection
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource[])} An array of collection's pages
   */
  RestResource.prototype.expandAll = function(options) {
    return this.getAll(options)
      .then(function(recipes) {
        return Promise.props(_.transform(recipes, function(recipes, recipe, index) {
          recipes[index] = this.$resolveResource(recipe, options, 'GET');
        }.bind(this)));
      }.bind(this))
    ;
  };

  /**
   * @summary Expand the given links of the collection, but still separated by page
   * @param {Number} [size] Size of the requested pages
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource[])} An array of collection's pages
   */
  RestResource.prototype.expandAllByPage = function(size, options) {
    return this.getAllByPage(size, options)
      .reduce(function(acc, recipesChunk, index) {
        return recipesChunk.expand().then(function(recipes) {
          acc[index] = recipes;
          return acc;
        });
      }, {})
    ;
  };

  /**
   * @summary Populate links of the current resource
   * @param {String|Array} [linkName] Name or names of links to populate. If none, populate all links by default
   * @param {Object} [options] Options for the HTTP call
   * @returns {Promise(RestResource)} The populated RestResource
   */
  RestResource.prototype.populate = function(linkName, options) {
    if (_.isObject(linkName) && !_.isArray(linkName)) {
      options = linkName;
      linkName = undefined;
    }

    var linkNames = !linkName ? _.keys(this.$links) :
      _.intersection(_.isArray(linkName) ? linkName : [linkName], _.keys(this.$links));

    return Promise.each(linkNames, function(linkName) {
      return this.follow(linkName, options)
        .then(function(linkValue) {
          if (!linkValue.$isCollection) {
            return linkValue;
          }
          return linkValue.expandAll(options);
        })
        .then(function(expandedLinkValue) {
          this[linkName] = expandedLinkValue;
        }.bind(this))
      ;
    }.bind(this)).return(this);
  };

  BRest.RestResource = RestResource;
};
