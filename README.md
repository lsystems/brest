BRest
================

Browser REST API client, serenely use your APIs, it handles the REST (whatever version and customized it is).

### Installation
#### Browser
````
npm install brest
````

Then:

````js
<script src="node_modules/brest/brest.js"></script>
````

#### Node.js
````
npm install brest
````

Then:

````js
var BRest = require('brest');
````

*!!! Important !!!* Note that *node* version cannot use internal `BRest.$sendRequest` function as-is because it uses XMLHttpRequest. Please patch `BRest.$sendRequest` or `global.XMLHttpRequest`.

#### Getting started
Follow the installation steps, then instanciate your first BRest API:

````js
BRest.RestApi('http://localhost:9000', { /* here go your options */ }).then(function(api) {
  // use your API
});
````

Access to your collection:
````js
api.follow('recipes').then(function(recipes) {
  // go to next recipe page
  return recipes.next();
}).then(function(recipes) {
  // get the 8th recipe
  return recipes.get(8);
});
````

#### API Reference
Please, see the [Api Reference](./API.md) or here [HTML Api Reference](http://brestproject.bitbucket.org/)

#### Do you want more?
Here is our [Trello board](https://trello.com/b/XyOShuLY/brest), taking in count [api-benchmark](https://bitbucket.org/lsystems/api-benchmark) tickets too.

### Development
#### Installation
```
hg clone ssh://hg@bitbucket.org/lsystems/brest
cd brest
npm install
gulp
```

#### Commands
````
gulp [watch]
````
Launch code style, test, build and watch your files

````
gulp test
````
Test your files using Mocha

````
gulp build
````
Build your lib in the dist repository, without minification

````
gulp build-all
````
Build your lib in the dist repository, with minified version

````
gulp codestyle
````
Check your files for code style using JSHint and JSCS

````
gulp cov
````
Generate a code coverage report in the `reports/` directory

````
gulp doc
````
Generate the markdown documentation

````
gulp html-doc
````
Generate the HTML documentation

````
cd doc/BRest
rm -rf {#version}
hg clone https://BrestProject@bitbucket.org/BrestProject/brestproject.bitbucket.org {#version}
cd ../..
gulp html-doc
cd doc/BRest/{#version}
hg add .
hg commit -m "Update doc"
hg push https://BrestProject@bitbucket.org/BrestProject/brestproject.bitbucket.org
````
Generate the HTML documentation (change {#version} by the current version) and publish it
