<a name="module_BRest"></a>
## BRest
BRest - Browser REST
Simply use REST v2, v3 or v4 API


* [BRest](#module_BRest)
    * [.RestResource](#module_BRest.RestResource)
        * [.follow(linkName, [options])](#module_BRest.RestResource+follow) ⇒ <code>Promise(RestResource)</code>
        * [.expand([options])](#module_BRest.RestResource+expand) ⇒ <code>Promise(RestResource[])</code>
        * [.next([size], [options])](#module_BRest.RestResource+next) ⇒ <code>Promise(RestResource)</code>
        * [.prev([size], [options])](#module_BRest.RestResource+prev) ⇒ <code>Promise(RestResource)</code>
        * [.previous()](#module_BRest.RestResource+previous)
        * [.first([size], [options])](#module_BRest.RestResource+first) ⇒ <code>Promise(RestResource)</code>
        * [.last([size], [options])](#module_BRest.RestResource+last) ⇒ <code>Promise(RestResource)</code>
    * [.RestApi([url], [options])](#module_BRest.RestApi) ⇒ <code>Promise(RestResource)</code>

<a name="module_BRest.RestResource"></a>
### BRest.RestResource
Default REST resource behaviors common to each API version

**Kind**: static class of <code>[BRest](#module_BRest)</code>  

* [.RestResource](#module_BRest.RestResource)
    * [.follow(linkName, [options])](#module_BRest.RestResource+follow) ⇒ <code>Promise(RestResource)</code>
    * [.expand([options])](#module_BRest.RestResource+expand) ⇒ <code>Promise(RestResource[])</code>
    * [.next([size], [options])](#module_BRest.RestResource+next) ⇒ <code>Promise(RestResource)</code>
    * [.prev([size], [options])](#module_BRest.RestResource+prev) ⇒ <code>Promise(RestResource)</code>
    * [.previous()](#module_BRest.RestResource+previous)
    * [.first([size], [options])](#module_BRest.RestResource+first) ⇒ <code>Promise(RestResource)</code>
    * [.last([size], [options])](#module_BRest.RestResource+last) ⇒ <code>Promise(RestResource)</code>

<a name="module_BRest.RestResource+follow"></a>
#### restResource.follow(linkName, [options]) ⇒ <code>Promise(RestResource)</code>
**Kind**: instance method of <code>[RestResource](#module_BRest.RestResource)</code>  
**Summary**: Follow a given link and return the related resource  
**Returns**: <code>Promise(RestResource)</code> - Followed resource  
**Throws**:

- <code>Error</code> if the link doesn't exist for the current resource


| Param | Type | Description |
| --- | --- | --- |
| linkName | <code>String</code> | Link name or resource name (non-HATEOAS) of the resource to load |
| [options] | <code>Object</code> | Options for the HTTP call |

**Example** *(Simple example to access to github repositories)*  
```js
BRest.RestApi('https://api.github.com').then(function(restApiResource) {
  return restApiResource.follow('user/repos');
}).then(function(repoCollectionResource) {
  // Here you can use your repository collection
});
```
<a name="module_BRest.RestResource+expand"></a>
#### restResource.expand([options]) ⇒ <code>Promise(RestResource[])</code>
**Kind**: instance method of <code>[RestResource](#module_BRest.RestResource)</code>  
**Summary**: Expand all resources of this collection resource page  
**Returns**: <code>Promise(RestResource[])</code> - Expanded resources  
**Throws**:

- <code>Error</code> if the current resource is not a collection


| Param | Type | Description |
| --- | --- | --- |
| [options] | <code>Object</code> | Options for the HTTP calls |

**Example** *(Simple example to get all github repositories of the first page of the collection)*  
```js
BRest.RestApi('https://api.github.com').then(function(restApiResource) {
  return restApiResource.follow('user/repos');
}).then(function(repoCollectionResource) {
  return repoCollectionResource.expand();
}).then(function(expandedRepos) {
  // Here you have access to all your repositories (limited to the first page of the collection, if paginated)
});
```
<a name="module_BRest.RestResource+next"></a>
#### restResource.next([size], [options]) ⇒ <code>Promise(RestResource)</code>
**Kind**: instance method of <code>[RestResource](#module_BRest.RestResource)</code>  
**Summary**: Navigate over the collection to the next collection page  
**Returns**: <code>Promise(RestResource)</code> - The next collection page or the current resource if it is the last page  

| Param | Type | Description |
| --- | --- | --- |
| [size] | <code>Number</code> | It determines the requested length of a collection chunk |
| [options] | <code>Object</code> | Options for the HTTP call |

**Example** *(Simple example to access to github repository next page using a pagination of 5)*  
```js
BRest.RestApi('https://api.github.com').then(function(restApiResource) {
  return restApiResource.follow('user/repos');
}).then(function(repoCollectionResource) {
  return repoCollectionResource.next(5);
}).then(function(lastRepoPageResource) {
  // Here you can use your repository collection second page using a pagination of 5
});
```
<a name="module_BRest.RestResource+prev"></a>
#### restResource.prev([size], [options]) ⇒ <code>Promise(RestResource)</code>
**Kind**: instance method of <code>[RestResource](#module_BRest.RestResource)</code>  
**Summary**: Navigate over the collection to the previous collection page  
**Returns**: <code>Promise(RestResource)</code> - The previous collection page or the current resource if it is the first page  

| Param | Type | Description |
| --- | --- | --- |
| [size] | <code>Number</code> | It determines the requested length of a collection chunk |
| [options] | <code>Object</code> | Options for the HTTP call |

**Example** *(Simple example to access to github repository next page using a pagination of 5)*  
```js
BRest.RestApi('https://api.github.com').then(function(restApiResource) {
  return restApiResource.follow('user/repos');
}).then(function(repoCollectionResource) {
  return repoCollectionResource.next(10);
}).then(function(repoCollectionResource) {
  return repoCollectionResource.prev(5);
}).then(function(lastRepoPageResource) {
  // Here you can use your repository collection second page using a pagination of 5
});
```
<a name="module_BRest.RestResource+previous"></a>
#### restResource.previous()
**Kind**: instance method of <code>[RestResource](#module_BRest.RestResource)</code>  
**See**: module:BRest.RestResource#prev  
<a name="module_BRest.RestResource+first"></a>
#### restResource.first([size], [options]) ⇒ <code>Promise(RestResource)</code>
**Kind**: instance method of <code>[RestResource](#module_BRest.RestResource)</code>  
**Summary**: Navigate over the collection to the first collection page  
**Returns**: <code>Promise(RestResource)</code> - The first collection page or the current resource if already on the first page  

| Param | Type | Description |
| --- | --- | --- |
| [size] | <code>Number</code> | It determines the requested length of a collection chunk |
| [options] | <code>Object</code> | Options for the HTTP call |

**Example** *(Simple example to access to github repository first page paginated by 5)*  
```js
BRest.RestApi('https://api.github.com').then(function(restApiResource) {
  return restApiResource.follow('user/repos');
}).then(function(repoCollectionResource) {
  return repoCollectionResource.first(5);
}).then(function(firstRepoPageResource) {
  // Here you can use your repository collection first page using a pagination of 5
});
```
<a name="module_BRest.RestResource+last"></a>
#### restResource.last([size], [options]) ⇒ <code>Promise(RestResource)</code>
**Kind**: instance method of <code>[RestResource](#module_BRest.RestResource)</code>  
**Summary**: Navigate over the collection to the last collection page  
**Returns**: <code>Promise(RestResource)</code> - The last collection page or the current resource if already on the last page  

| Param | Type | Description |
| --- | --- | --- |
| [size] | <code>Number</code> | It determines the requested length of a collection chunk |
| [options] | <code>Object</code> | Options for the HTTP call |

**Example** *(Simple example to access to github repository last page using a pagination of 5)*  
```js
BRest.RestApi('https://api.github.com').then(function(restApiResource) {
  return restApiResource.follow('user/repos');
}).then(function(repoCollectionResource) {
  return repoCollectionResource.last(5);
}).then(function(lastRepoPageResource) {
  // Here you can use your repository collection last page using a pagination of 5
});
```
<a name="module_BRest.RestApi"></a>
### BRest.RestApi([url], [options]) ⇒ <code>Promise(RestResource)</code>
**Kind**: static method of <code>[BRest](#module_BRest)</code>  
**Summary**: Instanciate your API root  
**Returns**: <code>Promise(RestResource)</code> - Your REST API resource to manipulate or the unexpected HTTP error  
**Throws**:

- <code>Error</code> if no URL or rootUrl option is given


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [url] | <code>String</code> |  | Your API root/base URL. It is the URL of your API summary. If you use an API that have no summary/root (like REST v2 API), it is facultative if you provide the `rootUrl` option. Otherwise, it is required. |
| [options] | <code>Object</code> |  | Options that are used when sending an HTTP request or passed to your RestResource |
| [options.hasSlash] | <code>Boolean</code> &#124; <code>Array.&lt;String&gt;</code> | <code>false</code> | Define if all or some API URIs contain a trailing slash (using their ids) for non-HATEOAS APIs |
| options.headers | <code>Object</code> |  | Those are the HTTP headers you want to send as a Map (key, value). It can be useful to add some generic headers like security headers to all your request. |
| options.linkPrefix | <code>String</code> |  | This prefix is used by some API as a way to differenciate links from standard keys |
| options.params | <code>Object</code> |  | Those are the HTTP parameters you want to send as a Map (key, value). Override HTTP parameters written in the url. It is useful to add some generic parameters through all your requests. |
| options.parser | <code>function</code> |  | It is used to parse your API body data if provided. `JSON.parse` is used by default. |
| options.rootUrl | <code>String</code> |  | Your root URL if your API doesn't have a summary (avoid a useless HTTP call). if you use an API that have no summary/root (like REST v2 API), you can provide this `rootUrl` option to avoid a useless API determination call to your unexisting API root. It overrides `url` parameter if both are set. |
| [options.verbose] | <code>Boolean</code> | <code>false</code> | If enabled, log a message when your request is sent, is successfully sent or is in error |

**Example** *(Simple example to start using Github API)*  
```js
BRest.RestApi('https://api.github.com').then(function(restApiResource) {
  // Here you can use your github API
});
```
**Example** *(Add &#x60;Authorization&#x60; header to each call with the Github API and always add &#x60;state=closed&#x60; to the URL. Moreover, each call will be logged in the console.)*  
```js
BRest.RestApi('https://api.github.com', {
  verbose: true,
  headers: {
    Authorization: 'Token 1546541fdg454654',
  },
  params: {
    state: 'closed',
  },
}).then(function(restApiResource) {
  // Here you can use your github API
});
```
**Example** *(V2 API with no summary)*  
```js
BRest.RestApi({
  rootUrl: 'https://api.myoldapi.com',
}).then(function(restApiResource) {
  // Here you can use your oldapi API
});
```
